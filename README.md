MPChem Astrochemical modelling tool
===================================

This is a multi-processing chemical code written in modern fortran and 
parallelized with MPI. MPChem aims to offer high performance for large 
parameter studies and post-processing of fluid elements with time 
dependent physical conditions.

Requirements
------------

- FORTRAN 2003+ compiler
- GNU Make (4.1+)
- BLAS
- ATLAS
- HDF5


Building and installation
-------------------------

The following environment variables need to be defined:

HDF5_HOME or HDF5_LIB and HDF5_INCL. If HDF5_HOME is set, then the later two 
variables are determined in the make script. Typically the HDF5_HOME variable 
is set on HPC clusters (e.g. by the module system). On Fedora 30 use the 
following:

```bash
export HDF5_INCL=/usr/lib64/gfortran/modules
export HDF5_LIB=/usr/lib64/gfortran/modules
```

On Ubuntu 18.04 use the following:

```bash
# Serial mode
export HDF5_HOME=/usr/lib/x86_64-linux-gnu/hdf5/serial
```

If the code is compiled on a computer where the MLK library is available, then 
MKL_HOME should be defined. The use should take care of importing the mkl (-mkl)
library instead of blas and lapack (-lblas -llapack).

Compile code and create a symbolic link to the mpchem binary in the 
users `~/bin` directory:
 
```bash
cd src
make & make install
```

In order to test the build process and basic functionality use pytest and 
the `code_test.py` file:

```bash
pytest-3 -v 
```

Reaction types
--------------

```
Str Rtype Formul Dep Name
 CP     1      1   n Direct Cosmic-Ray ionisation (CRP)
 CR     2      1   y Cosmic-Ray induced photo-ion.(CRPHOT)
 PH     3      2   n FUV ionisation and dissocation
 NN     4      3   n Neutral-Neutral
 CE     5      3   n Charge Exchange
 RA     6      3   n Radiative Association
 AD     7      3   n Associative Detachment
 RR     8      3   n Radiative Recombination
 DR     8      3   n Dissociative Recombination
 CD     ?      3   n Collisional Dissociation
 IN     ?      3   n Ion-Neutral
 MN     ?      3   n Mutual Neutralisation
REA     ?      3   n Radiative Electron Attachment
 IC    99      3   y Adsorption to grain surface
 DT    15      3,  y Thermal desorption
 DC    15      3,  y CRP induced desorption
 DP    15      3,  y Photodesorption
 GG    14      3,  y Grain surface reaction
```

Rate coefficients
-----------------

Formul = 3: Arrhenius like
```
a_k = alpha * ( T / 300.0 )**beta * EXP( -gamma / T )
```
Formul = 4: ionpol1
```
a_k = alpha * beta * ( 0.62D0 + 0.4767D0 * gamma \
    * ( 300.0D0 / T )**0.5 )
```
Formul = 5: ionpol2
```
a_k = alpha * beta * (1.0 + 0.0967 * gamma \
    * ( 300.0D0 / T )**0.5 + ( gamma**2 / 10.526D0 ) \
    * 300.0D0 / T )
```
Formul = 1: Cosmic rays

```
a_k = alpha * Zeta_CR
```
Formul = 2: FUV photons
```
a_k = alpha * (fShield_St * dfSt * G0_St \
            + fShield_IS * dfIS * G0_IS)
```
Formul = : H2 formation (Wakelam+ 2015, Nautilus)
```
gH + gH --> H2
a_k = alpha * 1.186D7 * EXP( 225.D0 / T )**(-1) \
    * GTODN / gdens

H  + g --> gH
a_k = alpha * ( T / 300.0D0 )**beta * gdens / GTODN
```
Formul = : H2 formation (Cazeaux & Tielens, 2002, 2004)
```
H + H --> H2
a_k = 0.5 * grain_radius**2 * ddens * Vth * stick * eps_H2
```
