!
! This file is part of the MPChem / pychemic project.
! 
! Copyright (C) 2018 Laszlo Szucs <laszlo.szucs@mpe.mpg.de>
! 
! This program is free software; you can redistribute it and/or
! modify it under the terms of the GNU General Public License as
! published by the Free Software Foundation; version 2 of the License.
! 
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!

MODULE param_module
  !
  ! This module contains parameters and variables that 
  ! are accessable in the complete program.
  !
  ! The defulat values can be set for each parameter and/or
  ! the parameter values can be read from configuration
  ! files.
  !
  ! IMPORTANT: if you change param_module then update bcast_param() routine
  !            in mpi_com.F90.
  !
  ! TODO: read parameters from command line
  !
  IMPLICIT NONE
  !
  SAVE
  !
  ! TODO: move elsewhere
  INTEGER (kind=4) :: stdo
  CHARACTER (len=132), PARAMETER :: ver = '0.17 (17.04.2018)' ! TODO: replace with git ver.
  !
  INTEGER (kind=4) :: istart, iend
  INTEGER (kind=4) :: verbose, healpix_npix
  INTEGER (kind=4) :: meth_flag
  LOGICAL :: USE_NL99, USE_CHARGE_CONS, USE_GRAIN_CHEM
  LOGICAL :: OIDL, ODAT, OBIN, OHDF, USE_HDF5
  LOGICAL :: USE_THERM_DES, USE_PHOT_DES, USE_CRP_DES
  LOGICAL :: USE_QT_DIFF, USE_QT_RBAR, USE_COULOMB_FOC, USE_MOD_RATE
  LOGICAL :: USE_HEALPX, USE_COMB_SHIELD, USE_XRSPEC, USE_UVSPEC, USE_ECOLI
  CHARACTER (len=12), DIMENSION(:), ALLOCATABLE :: qt_diff_spec, qt_rbar_spec
  CHARACTER (len=12), DIMENSION(:), ALLOCATABLE :: mod_rate_spec
  REAL (kind=8) :: relerr, abserr    ! Relative and absolute errors for the solver
  REAL (kind=8) :: UV_scale, CRP_scale, X_scale, ZetaRN
  REAL (kind=8) :: amu
  REAL (kind=8) :: grain_radius, grain_rho_bulk, dust_to_gas, &
                   ed_mod, stoch_lim, sprod0, G_CR, phot_des_yield0, &
                   surf_site_dens, XR_BOUND_EV, grain_sublim_temp
  REAL (kind=8) :: T_floor, T_diff_tol
  CHARACTER (len=90) :: temp_dep
  CHARACTER (len=90) :: surf_des
  CHARACTER (len=90) :: species_file, reaction_file, xrsig_file, uvsig_file
  CHARACTER (len=90) :: out_base_name, ecoli_file
  CHARACTER (len=90) :: result_folder, debug_folder
  INTEGER (kind=4), DIMENSION(:), ALLOCATABLE :: dep_rtype
  !
  ! Cazaux & Tielens H2 formation model parameters
  REAL (kind=8) :: E_H2, E_S, E_Hp, E_Hc, barrier_width_cazaux, &
                   surf_frac_cazeux
  !
  ! Initial abundances
  CHARACTER (len=12), DIMENSION(:), ALLOCATABLE :: init_spec
  REAL (kind=8), DIMENSION(:), ALLOCATABLE :: init_abuns
  INTEGER (kind=4) :: nr_init_spec
  !
  ! Set access to subroutines and functions
  !
  PUBLIC :: set_param_defaults, set_init_abuns_defaults, read_param_file, &
            read_init_abuns
  !
CONTAINS
  !
  ! Parameter defaults and reading related routines
  !
  SUBROUTINE set_param_defaults()
    IMPLICIT NONE
    !
    ! Set default values
    ! Note: these should be overwritten by 0io.inp controll file!
    !
    ! Control
    !
    stdo   = 6         ! default output to screen
    istart = 1         ! starting cell
    iend   = 32767     ! maximum representable value at INT(kind=2)
    ! Error of ith species: error_i = relerr * n_i + abserr
    relerr = 1.0D-04   ! relative error of solver
    abserr = 1.0D-24   ! absoulte error of solver
    ! TODO: move to model specific parameter!
    USE_HEALPX = .FALSE.   ! environment input contains HEALPix data
    USE_COMB_SHIELD = .FALSE. ! line and dust shielding combined
    !
    healpix_npix = 48  ! Number of pixels in the HEALpix projection
    !
    ! Output
    !
    result_folder = 'results'   ! default output folder (within run folder)
    debug_folder = 'debug_info' ! default debug file output folder
    out_base_name = 'kida2014'  ! default base output name
    verbose = 3        ! Print detailed and debugging data?
    OIDL    = .TRUE.   ! write IDL and python readable output?
    ODAT    = .TRUE.   ! write human readable output?
    OBIN    = .TRUE.   ! write binary output?
    OHDF    = .FALSE.  ! write HDF5 output?
    USE_HDF5 = .FALSE. ! Use HDF5 input
    !
    ! Solver
    !
    meth_flag = 29     ! Method flag, first digit: 1 (non-stiff) or 2 (stiff system)
                       ! second digit: 1 (general) or 9 (preconditioned)
    !
    ! Chemical network
    !
    species_file    = 'rspecies_kida2014.dat'    ! default species file name
    reaction_file   = 'rreactions_kida2014.dat'  ! default reaction file name
    ecoli_file      = 'cfit_voronov.dat'         ! electr. col. ion. fit data file
    USE_NL99        = .FALSE.    ! network contains Nelson \& Langer (1998) reactions?
    xrsig_file      = 'xray_cross_Verner95.hdf5' ! default X-ray cross section file
    uvsig_file      = 'uv_cross_Heays17.hdf5'    ! default X-ray cross section file
    USE_CHARGE_CONS = .FALSE.    ! force charge conservation?
    USE_COULOMB_FOC = .TRUE.     ! Coulomb focusing following the Draine \& Sutin (1987)
    temp_dep        = 'limit'    ! how to handle temperature dependent reactions?
    T_floor         = 0.00D0     ! floor temperature in Kelvin
    T_diff_tol      = 100.0D0    ! dT = [T - Treac(min,max)], if dT > T_diff_tol: reject reaction   
    !
    ! Note: the following factors scale the parameters given in the environment
    !       input file (typically  1environ.inp)
    UV_scale        = 1.00D0     ! interstellar radiation field scaling factor
    CRP_scale       = 1.00D0     ! cosmic ray ionisation rate scaling factor
    X_scale         = 1.00D0     ! X-ray radiation scaling factor
    USE_XRSPEC      = .FALSE.    ! Read and use frequency dependent X-ray cross
                                 ! sections from HDF5 file
    USE_UVSPEC      = .FALSE.    ! Read and use frequency dependent UV cross
                                 ! sections from  HDF5 file
    USE_ECOLI       = .FALSE.    ! Read and use electron collisional ionisation 
                                 ! rate fit of Voronov (1997). If .TRUE. user
                                 ! must provide ecoli_file.
    XR_BOUND_EV     = 100.0D0    ! Boundary between UV and X-ray range in eV
                                 ! FUV and X-ray cross sections are set to 0.0
                                 ! below and above XR_BOUND_EV, respectively.
    !
    ZetaRN          = 0.00D0     ! Ionization rate due to radioactive decay
    !
    amu             = 1.4D0      ! Mean molecular weight
    ALLOCATE( dep_rtype(4) )
    dep_rtype = (/2,14,15,66/)   ! Dependent reaction rtype index
    !
    ! Grain surface processes
    !
    USE_GRAIN_CHEM  = .FALSE.  ! network file need to contain grain reactions it TRUE 
    USE_THERM_DES   = .TRUE.   ! include thermal desorption
    USE_PHOT_DES    = .TRUE.   ! include photon induced desorption
    USE_CRP_DES     = .TRUE.   ! include cosmic ray induced desorption
    USE_QT_DIFF     = .FALSE.  ! include quantum tunneling for diffusion on surface
    USE_QT_RBAR     = .FALSE.  ! include quantum tunneling over reaction barriers
    USE_MOD_RATE    = .FALSE.  ! use modified rate on the grain surface
    grain_radius    = 1.0D-5   ! grain radius [cm]
    grain_rho_bulk  = 3.00D0   ! dust density [g/cm^3]
    grain_sublim_temp = 1200.0 ! grain sublimation temperature [K]
    surf_site_dens  = 1.5e+15  ! surface site density in 1/cm^2
    dust_to_gas     = 1.0D-2   ! dust-to-gas mass ratio
    ed_mod          = 0.55D0   ! ratio between desorption and diffusion energies
    stoch_lim       = 1.0D5    ! TODO: remove
    sprod0          = 1.00D0   ! fraction of product remaining on surface
    G_CR            = 1.0D-5   ! CR induced photon flux in unit of G0
    surf_des        = 'USER'   ! desorption energy & tunneling
    phot_des_yield0 = 1.00D-5  ! photodesorption yield if not set in network
    !
    ! TODO: make these user selectable
    ALLOCATE( qt_diff_spec(1), qt_rbar_spec(2) )
    qt_diff_spec(1) = 'gH          '
    qt_rbar_spec(1:2) = (/ 'gH          ', 'gH2         ' /)
    ! Modify surface rates of selected species
    ALLOCATE( mod_rate_spec(14) )
    mod_rate_spec(1:14) = (/ 'gH          ', 'gH2         ', 'gHe         ',&
                             'gC          ', 'gN          ', 'gO          ',&
                             'gS          ', 'gSi         ', 'gFe         ',&
                             'gNa         ', 'gMg         ', 'gP          ',&
                             'gF          ', 'gCl         ' /)
    !
    ! Cazaux & Tielens H2 formation model parameters
    E_H2            = 320.0    ! Desorption energy of H2 [K]
    E_Hp            = 600.0    ! Desorption energy of physisorbed atomic hydrogen
    E_Hc            = 10000.0  ! Desorption energy of chemisorbed atomic hydrogen
    E_S             = 200.     ! Energy of saddle point between chemisorbed and 
                               ! physisorbed sites [K]
    barrier_width_cazaux = 3.0D-8 ! Width of potential barrier between chem. and 
                                  ! phys. binding sites. [cm]
    surf_frac_cazeux = 0.005   ! Fraction of formed H2 that remains on the grain
    !
  END SUBROUTINE set_param_defaults
  !
  SUBROUTINE set_init_abuns_defaults()
    !
    ! This routine sets the default initial abundances to be 
    ! used for the chemical model, if initial abundance input
    ! file is not found.
    !
    ! The below set abundaces resemble the so-called `low-metal`
    ! abundance set from Lee et al. (1998, A&A, 334, 1047) 
    ! with hydrogen fully converted to molecules.
    !
    ! This should be representative of young prestellar cores, 
    ! which are depletted in heavy elements.
    ! This initial abundance set is also used in proto-planetary
    ! disk models some times.
    !
    ! These initial abundances are used also in the KIDA papers
    ! (Wakelam et al., 2015, ApJS, 217, 20)
    !
    IMPLICIT NONE
    !
    nr_init_spec = 15
    !
    ALLOCATE( init_spec(nr_init_spec), init_abuns(nr_init_spec) )
    !
    init_spec(1:nr_init_spec) =  &
           (/ 'G0    ', 'H2    ', 'HE    ', 'C+    ',  & 
              'N     ', 'O     ', 'S+    ', 'SI+   ', &
              'NA+   ', 'MG+   ', 'FE+   ', 'P+    ',  &
              'CL+   ', 'F     ', 'ELECTR' /)
    init_abuns(1:nr_init_spec) = &
           (/ 1.322D-12, 5.000D-01, 9.000D-02, 1.700D-04,  &
              6.200D-05, 2.400D-04, 8.000D-08, 8.000D-09,  &
              2.000D-09, 7.000D-09, 3.000D-09, 2.000D-10,  &
              1.000D-09, 6.680D-09, 1.701D-04 /)
    !
  END SUBROUTINE set_init_abuns_defaults
  !
  SUBROUTINE read_param_file(input)
    !
    ! This routine reads a configuration file and sets code 
    ! parameters according the content of the file. 
    !
    ! The config file should follow the below format:
    !
    !  ; The following variable is useful:
    !  var1   = 5.74d0  ; Here some comments
    !
    ! Variables set in the configuration file will overwrite 
    ! default values (if set by the set_config_defaults() 
    ! routine).
    !
    ! The list and default value of the currently recognized 
    ! variabels is the following:
    !
    !   use_nl99        =  network file contains hard coded 
    !                      reactions used in the NL99 network 
    !                      (Szucs et al. 2014); (Default=0)
    !   use_charge_cons =  if eq. 1 force charge conservation
    !                      set automatically true if use_nl99
    !                      is set. (Default=0)
    !   use_grain_chem  =  if eq. 1 model grain surface processes
    !                      (Note that network file should contain
    !                      grain surface reaction). (Default=0)
    !   use_therm_des   =  if eq. 1 switch thermal desorption from
    !                      grain surface ON. (Default=1)
    !   use_phot_des    =  if eq. 1 switch photodesorption from 
    !                      grain surface ON. (Default=1)
    !   use_crp_des     =  if eq. 1 switch cosmic ray induced 
    !                      desorption from grain ON. (Default=1)
    !   use_qt_diff     =  if eq. 1 allow quantum tunnelling diffustion
    !                      on grain surface. (Default=0)
    !   use_qt_rbar     =  if eq. 1 allow quantum tunnelling over
    !                      reaction barriers for grain surface 
    !                      reactions. (Default=0)
    !   use_mod_rate    =  if eq. 1 modify the surface diffusion rate of 
    !                      grain surface reactions (Following Semenov et al. 
    !                      and the Wakelam et al.)
    !   use_coulomb_foc =  if eq. 1 use Coulomb focusing when 
    !                      computing the ion recombination rate
    !                      with charge grains. (Draine \& Sutin 1987)
    !                      (Default=1)
    !   verbose         =  set the verbosity of the code (0=only errors,
    !                      ... 4=all output). Default=3
    !   oidl            =  output IDL / python readable file with 
    !                      detailed content (phys. cond., abuns., rates,...)
    !                      (Default=1)
    !   odat            =  output human readable file (less content)
    !                      (Default=1)
    !   obin            =  output binary data file (less content, designed
    !                      for large models). (Default=1)
    !   ohdf            =  output HDF5 data file (contains physical conditions, 
    !                      result abundances and reaction rate coefficients).
    !                      (Default=0)
    !   use_hdf5        =  use HDF5 format for reading model physical 
    !                      conditions and writing results to disk. (Default=0)
    !   use_healpx      =  set to 1, if physical condition input
    !                      contains HEALPix data (48 pixel). (Default=0)
    !   use_comb_shield =  set to 1, if fH2 and fCO shielding coefficients
    !                      (in the environment input) combine line and 
    !                      dust continuum shielding.
    !   temp_dep        =  string setting the treatment of reactions with 
    !                      temperature validity ranges (possible values: 
    !                      'limit', 'extrap', 'reject'; Default='limit')
    !   t_floor         =  double, floor gas temperature (K) used in reaction 
    !                      rate calculation (Default=0.0d0)
    !   t_diff_tol      =  double, tolerated temperature difference between 
    !                      Tgas and the temperature limits of reaction.
    !                      (Default=100 K)
    !   species_file    =  string name of the file with the species names
    !                      (Default='rspecies_kida2014.inp')
    !   reaction_file   =  string name of the file with the reaction network
    !                      (Default='rreactions_kida2014.inp')
    !   ecoli_file      =  string name of the electron collisional ion. data file
    !                      (Default='cfit_voronov.dat')
    !   result_folder   =  strint, results will be written into this folder
    !                      (relative to the run directory, Default='results')
    !   debug_folder    =  string, debug information is stored in this folder. 
    !                      Folder is created if it does not exist. (relative to 
    !                      the run directory, Default='debug_info'). Note that 
    !                      debug is written only if -DDEBUG compiler option is 
    !                      set
    !   out_base_name   =  string setting the base name of outputs 
    !                      (Default='kida2014')
    !   relerr          =  double number setting the relative error in the 
    !                      solver (Default=)
    !   abserr          =  double number setting the absolute error in the
    !                      solver (Default=)
    !   istart          =  index of first cell to be run in the code
    !                      (Default=1)
    !   iend            =  index of last cell to be run in the code
    !                      (Default=32767)
    !   stdo            =  unit used for output and code messages
    !                      (Default=6, screen)
    !   UV_scale        =  double number used to scale the interstellar radiation
    !                      field strength. This scales values given in the phys.
    !                      model. (Default=1.0)
    !   CRP_scale       =  double number used to scale the cosmic ray ionisation
    !                      rate. This scales values given in the phys.
    !                      model. (Default=1.0)
    !   X_scale         =  double number used to scale the X-ray ionisation
    !                      rate. (Default=1.0)
    !   ZetaRN          =  double number, ionization rate due to radioactive
    !                      decay. Usually relevant in protoplanetary disks.
    !                      (Default=0.0)
    !   use_xrspec      =  read and use X-ray cross section from HDF5 input file
    !                      (Default=0)
    !   use_uvspec      =  read and use UV cross section from HDF5 input file
    !                      (Default=0)
    !   use_ecoli       =  read and use electron collisional ionisation rate fit
    !                      (Default=0)
    !   xr_bound_ev     =  double number, boundary between FUV and X-ray range 
    !                      in eV. (Default=100.0)
    !   amu             =  double number, mean molecular weight in proton mass
    !   grain_radius    =  double number, grain radius in [cm] (Default=1.0D-5) 
    !   grain_rho_bulk  =  double number, bulk grain density in [g/cm^3].
    !                      (Default=3.00D0)
    !   grain_sublim_temp = double, grain sublimation temperature in K. Above 
    !                       this temperature no grain process is possible.
    !                       (Default=1200.0)
    !   surf_site_dens  =  double number, surface site density in 1/cm^2
    !                      (Default=1.5D+15)
    !   dust_to_gas     =  double number, dust-to-gas mass ratio.
    !                      (Default=1.0D-2)
    !   stoch_lim       =  ?? ! TODO: remove or use stoch_lim
    !   ed_mod          =  double number, ratio between desorption and 
    !                      diffusion energies on grain. (Default=0.55)
    !   sprod0          =  double number, fraction of exothermic grain 
    !                      reaction products that remain on the grains.
    !                      (Default=1.00D0)
    !   G_CR            =  double number, CR induced photon flux in unit 
    !                      of G0. (Default=1.00D-5)
    !   phot_des_yield0 =  double, photodesorption yield if not set in network
    !                      (Default=1.00D-5)
    !   surf_des        =  
    !   E_H2            =  double, H2 desorption energy in the Cazauex & Tielens
    !                      (2004) H2 formation model [K]. (Default=320.0)
    !   E_Hp            =  double, physisorbed H desorption energy in the 
    !                      Cazauex & Tielens(2004) H2 formation model [K]. 
    !                      (Default=600.0)
    !   E_Hc            =  double, chemisorbed H desorption energy in the 
    !                      Cazauex & Tielens(2004) H2 formation model [K]. 
    !                      (Default=10000.0)
    !   E_S             =  double, energy of saddle point between chemisorbed 
    !                      and physisorbed sites [K]. (Default=200.)
    !   barrier_width_cazaux = double, width of potential barrier between 
    !                          chemisorbed and physisorbed binding sites in  
    !                          cm unit. (Default=3.0D-8)
    !   surf_frac_cazeux = double, fraction of formed H2 that remains on the 
    !                      grain surface. (Default=0.005)
    !
    ! Input parameter(s):
    !
    !   input (optional)=  string name of input configuration file 
    !                      (Default='0io.inp') 
    !
    USE namelist_module
    !
    IMPLICIT NONE
    !
    ! Local variables
    CHARACTER (len=*), INTENT(in), OPTIONAL :: input
    CHARACTER (len=90) :: input_file
    LOGICAL :: FILE_EXIST
    INTEGER (kind=4) :: tmp_int
    ! Initialize tmp_int
    tmp_int = -1
    !
    IF ( .NOT. PRESENT(input) ) THEN
       input_file = '0io.inp'
    ELSE
        input_file = input
    END IF
    !
    ! Check whether file exists
    INQUIRE( FILE=input_file, EXIST=FILE_EXIST )
    !
    IF ( FILE_EXIST ) THEN
       !
       64 FORMAT ("INFO: Input file ",(a)," found. Reading parameters...")
       WRITE(stdo,64,advance='no') TRIM(input_file)
       !
       OPEN (unit=01,file=TRIM(input_file),status='old', &
            access='sequential')
       CALL read_input_file()
       CLOSE (01)
       !
       CALL parse_input_integer('use_nl99@                     ',tmp_int)
       IF (tmp_int == 1) USE_NL99 = .TRUE. ; tmp_int = -1  ! reset tmp_int
       CALL parse_input_integer('use_charge_cons@              ',tmp_int)
       IF ( (tmp_int == 1) .or. use_nl99 ) USE_CHARGE_CONS = .TRUE. ; tmp_int = -1
       CALL parse_input_integer('use_grain_chem@               ',tmp_int)
       IF (tmp_int == 1) USE_GRAIN_CHEM = .TRUE. ; tmp_int = -1
       CALL parse_input_integer('use_therm_des@                ',tmp_int)
       IF (tmp_int == 0) USE_THERM_DES = .FALSE. ; tmp_int = -1
       CALL parse_input_integer('use_phot_des@                 ',tmp_int)
       IF (tmp_int == 0) USE_PHOT_DES = .FALSE. ; tmp_int = -1
       CALL parse_input_integer('use_crp_des@                  ',tmp_int)
       IF (tmp_int == 0) USE_CRP_DES = .FALSE. ; tmp_int = -1
       CALL parse_input_integer('use_qt_diff@                  ',tmp_int)
       IF (tmp_int == 1) USE_QT_DIFF = .TRUE. ; tmp_int = -1
       CALL parse_input_integer('use_qt_rbar@                  ',tmp_int)
       IF (tmp_int == 1) USE_QT_RBAR = .TRUE. ; tmp_int = -1
       CALL parse_input_integer('use_mod_rate@                 ',tmp_int)
       IF (tmp_int == 1) USE_MOD_RATE = .TRUE. ; tmp_int = -1
       CALL parse_input_integer('use_coulomb_foc@              ',tmp_int)
       IF (tmp_int == 0) USE_COULOMB_FOC = .FALSE. ; tmp_int = -1
       CALL parse_input_integer('oidl@                         ',tmp_int)
       IF (tmp_int == 0) OIDL = .FALSE. ; tmp_int = -1
       CALL parse_input_integer('odat@                         ',tmp_int)
       IF (tmp_int == 0) ODAT = .FALSE. ; tmp_int = -1
       CALL parse_input_integer('obin@                         ',tmp_int)
       IF (tmp_int == 0) OBIN = .FALSE. ; tmp_int = -1
       CALL parse_input_integer('ohdf@                         ',tmp_int)
       IF (tmp_int == 1) OHDF = .TRUE. ; tmp_int = -1
       CALL parse_input_integer('use_hdf5@                     ',tmp_int)
       IF (tmp_int == 1) USE_HDF5 = .TRUE. ; tmp_int = -1
       CALL parse_input_integer('use_xrspec@                   ',tmp_int)
       IF (tmp_int == 1) USE_XRSPEC = .TRUE. ; tmp_int = -1
       CALL parse_input_integer('use_uvspec@                   ',tmp_int)
       IF (tmp_int == 1) USE_UVSPEC = .TRUE. ; tmp_int = -1
       CALL parse_input_integer('use_ecoli@                    ',tmp_int)
       IF (tmp_int == 1) USE_ECOLI = .TRUE. ; tmp_int = -1
       CALL parse_input_integer('use_healpx@                   ',tmp_int)
       IF (tmp_int == 1) USE_HEALPX = .TRUE. ; tmp_int = -1
       CALL parse_input_integer('use_comb_shield@              ',tmp_int)
       IF (tmp_int == 1) USE_COMB_SHIELD = .TRUE. ; tmp_int = -1
       CALL parse_input_integer('healpix_npix@                 ',tmp_int)
       IF ( MODULO(tmp_int,12) .eq. 0 ) THEN
          healpix_npix = tmp_int
       ELSE
          ! TODO: Check whether it is valied pixel number:
          !       Npix = 12 * Nside**2 with Nside = 1, 2, 4, 8, ...
          CONTINUE
       END IF
       tmp_int = -1
       CALL parse_input_integer('verbose@                      ',verbose)
       CALL parse_input_word('temp_dep@                     ',temp_dep,tmp_int)
       tmp_int = -1
       CALL parse_input_word('species_file@                 ',species_file,tmp_int)
       tmp_int = -1
       CALL parse_input_word('reaction_file@                ',reaction_file,tmp_int)
       tmp_int = -1
       CALL parse_input_word('ecoli_file@                   ',ecoli_file,tmp_int)
       tmp_int = -1
       CALL parse_input_word('result_folder@                ',result_folder,tmp_int)
       tmp_int = -1
       CALL parse_input_word('debug_folder@                 ',debug_folder, tmp_int)
       tmp_int = -1
       CALL parse_input_word('out_base_name@                ',out_base_name,tmp_int)
       tmp_int = -1
       CALL parse_input_double('relerr@                       ',relerr)
       CALL parse_input_double('abserr@                       ',abserr)
       CALL parse_input_integer('istart@                       ',istart)
       CALL parse_input_integer('iend@                         ',iend)
       CALL parse_input_integer('stdo@                         ',stdo)
       CALL parse_input_double('uv_scale@                     ',UV_scale)
       CALL parse_input_double('crp_scale@                    ',CRP_scale)
       CALL parse_input_double('x_scale@                      ',X_scale)
       CALL parse_input_double('zeta_rn@                      ',ZetaRN)
       CALL parse_input_double('grain_radius@                 ',grain_radius)
       CALL parse_input_double('grain_rho_bulk@               ',grain_rho_bulk)
       CALL parse_input_double('surf_site_dens@               ',surf_site_dens)
       CALL parse_input_double('dust_to_gas@                  ',dust_to_gas)
       CALL parse_input_double('stoch_lim@                    ',stoch_lim)
       CALL parse_input_double('ed_mod@                       ',ed_mod)
       CALL parse_input_double('sprod0@                       ',sprod0)
       CALL parse_input_double('g_cr@                         ',G_CR)
       CALL parse_input_double('phot_des_yield0@              ',phot_des_yield0)
       CALL parse_input_double('amu@                          ',amu)
       CALL parse_input_double('t_floor@                      ',T_floor)
       CALL parse_input_double('t_diff_tol@                   ',T_diff_tol)    
       CALL parse_input_double('xr_bound_ev@                  ',XR_BOUND_EV)  
       CALL parse_input_double('e_h2@                         ',E_H2)
       CALL parse_input_double('e_hp@                         ',E_Hp)
       CALL parse_input_double('e_hc@                         ',E_Hc)
       CALL parse_input_double('e_s@                          ',E_S)
       CALL parse_input_double('barrier_width_cazaux@         ', &
                               barrier_width_cazaux)
       CALL parse_input_double('surf_frac_cazeux@             ', &
                               surf_frac_cazeux)
       CALL parse_input_double('grain_sublim_temp@            ', &
                               grain_sublim_temp)
       CALL parse_input_word('surf_des@                     ',surf_des,tmp_int)
       tmp_int = -1
       !
       CALL parse_input_integer('stdo@                         ',stdo)
       CALL parse_input_integer('meth_flag@                    ',tmp_int)
       ! Check meth_flag validity:
       IF ( ( tmp_int .eq. 21 ) .or. ( tmp_int .eq. 29 ) .or.  &
            ( tmp_int .eq. 11 ) .or. ( tmp_int .eq. 19 ) ) THEN
          meth_flag = tmp_int
          tmp_int = -1
       ELSE IF ( tmp_int .ne. -1) THEN
          WRITE(stdo,*)
          66 FORMAT ( "WARN: ",(a),(i4)," unknown. Defaulting to ",(i4),"." ) 
          WRITE(stdo,66) "meth_flag = ", tmp_int, meth_flag
          WRITE(stdo,'("INFO: Continue reading parameters...")',advance='no')
       END IF
       !
       WRITE(stdo,'(2x,a)') 'DONE'
       !
    ELSE
       !
       65 FORMAT ("WARN: Input file ",(a)," not found. Using default values!")
       WRITE(stdo,65) TRIM(input_file)
       !
    END IF
    !
  END SUBROUTINE read_param_file
  !
  SUBROUTINE read_init_abuns(input)
    !
    ! This routine reads initial abundances from file.
    ! If file not found then the default `low metal`
    ! are used.
    !
    ! Input parameter(s):
    !
    !   input (optional)=  string name of input abundance file 
    !                      (Default='3abunds.inp')
    !
    IMPLICIT NONE
    !
    ! Local variables
    CHARACTER (len=*), INTENT(in), OPTIONAL :: input
    CHARACTER (len=90) :: input_file
    CHARACTER (len=12) :: spec
    LOGICAL :: FILE_EXIST
    INTEGER (kind=4) :: i, last, nr_charge, i_electr
    REAL (kind=8) :: ion_abuns, electr_abuns
    !
    IF ( .NOT. PRESENT(input) ) THEN
       input_file = '3abunds.inp'
    ELSE
        input_file = input
    END IF
    !
    ion_abuns = 0.0
    electr_abuns = 0.0
    !
    ! Check whether file exists
    INQUIRE( FILE=input_file, EXIST=FILE_EXIST )
    !
    IF ( FILE_EXIST ) THEN
       !
       64 FORMAT ("INFO: Input file ",(a)," found. Reading initial abundances...")
       65 FORMAT ("WARN: Input file ",(a)," not found. Using low metal abuns. (Lee 1998)!")
       66 FORMAT ("WARN:", 1x,a,a,6x,a,1x,E12.5,2x,a,1x,E12.5,a,6x,a)
       
       WRITE(stdo,64,advance='no') TRIM(input_file)
       !
       OPEN (unit=01,file=TRIM(input_file),status='old', &
            access='sequential')
       READ (01,*)
       READ (01,*) nr_init_spec
       !
       IF ( ALLOCATED(init_spec) ) DEALLOCATE(init_spec) 
       ALLOCATE( init_spec(nr_init_spec) )
       IF ( ALLOCATED(init_abuns) ) DEALLOCATE(init_abuns) 
       ALLOCATE( init_abuns(nr_init_spec) )
       !
       DO i=1, nr_init_spec
          !
          READ (01,*) init_spec(i), init_abuns(i)
          !
          ! TODO: move this to separate function
          ! Count charges
          ! Initialization of boundaries of the species name:
          spec =init_spec(i)
          last = LEN(TRIM(spec))
          nr_charge = 0
          
          ! Ions: Iterate until all charges are removed
          10 CONTINUE
          IF (spec(last:last).EQ.'+') THEN
             last = last - 1
             nr_charge = nr_charge + 1
             GOTO 10
          ELSE IF (spec(last:last).EQ.'-') THEN
             last = last - 1
             nr_charge = nr_charge - 1
             GOTO 10
          END IF
          
          ion_abuns = ion_abuns + nr_charge * init_abuns(i)
          
          IF (TRIM(spec) .eq. 'ELECTR') THEN
             electr_abuns = init_abuns(i)
             i_electr = i
          END IF
          !
       END DO
       !
       CLOSE(01)
       !
       ! Check for charge conservation
       IF (ABS(electr_abuns-ion_abuns).gt.abserr) THEN
          !
          WRITE(stdo,*)
          WRITE(stdo,66, advance='no') &
                         "Charge is not conserved in initial abundances (within abserr):",&
                         NEW_LINE('A'), "init(electron) = ", electr_abuns,&
                         "init(ion) = ", ion_abuns, NEW_LINE('A'), &
                         "Correcting electron abundance!" 
          init_abuns(i_electr) = ion_abuns
          !
       END IF
       !
       WRITE(stdo,'(2x,a)') 'DONE'
       !
    ELSE
       !
       WRITE(stdo,65) TRIM(input_file)
       !
    END IF
    !
  END SUBROUTINE read_init_abuns
  !
END MODULE param_module
