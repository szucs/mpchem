!
! This file is part of the MPChem / pychemic project.
! 
! Copyright (C) 2018 Laszlo Szucs <laszlo.szucs@mpe.mpg.de>
! 
! This program is free software; you can redistribute it and/or
! modify it under the terms of the GNU General Public License as
! published by the Free Software Foundation; version 2 of the License.
! 
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!

PROGRAM mpchem
  !
  ! This is the main routine
  !
  USE constants
  USE param_module
  USE network_module
  USE physical_conditions
  USE hdf5io
  USE rates_module
#ifdef USE_MPI
  USE mpi
  USE mpi_com
#endif
  USE solve
  
  IMPLICIT NONE
  
  ! Local variables
  !
  ! Timing related variables
  REAL (kind=8) :: start_cpu_time, finish_cpu_time,           &
                   start_cpu_time_cell, finish_cpu_time_cell, &
                   start_cpu_time_task, finish_cpu_time_task
  !
  ! Structure containing the chemical model (physical conditions, 
  ! rate coefficients and abundances)
  TYPE(ChemModel), DIMENSION(:), ALLOCATABLE :: models
  INTEGER (kind=4) :: nr_cell, i, j
  !
#ifdef USE_MPI
  !
  ! MPI variables
  !
  INTEGER (kind=4) :: ierr, id_proc, nr_proc
  REAL (kind=8) :: wtime
  INTEGER (kind=4) :: nr_workers, nr_closed_workers
  INTEGER (kind=4) :: task_index, work_index
  INTEGER (kind=4) :: tag, input, stat, message, source
  INTEGER (kind=4), DIMENSION(MPI_STATUS_SIZE) :: status
  ! 
  ! ChemModel temporarily used on worker processes
  TYPE(ChemModel) :: worker_model
  !
#endif
  !
  ! Format codes for output on stdo (default output is screen)
  !
  64 FORMAT ( "=> START: SETTING/READING <",(a),">" )
  65 FORMAT ( "=> END: SETTING/READING <",(a),">" )
  66 FORMAT ( "==> Task time: ",f10.3," s" )
  67 FORMAT ( "==> Total time: ",f10.3," s" )

#ifdef USE_MPI
  !
  ! Initialize MPI.
  !
  CALL MPI_Init ( ierr )
  !
  ! Get the number of processes.
  CALL MPI_Comm_size ( MPI_COMM_WORLD, nr_proc, ierr )
  !
  ! Stop if trying to run serial
  IF (nr_proc .lt. 2) THEN 
     WRITE(stdo,*) "ERROR: nr_proc < 2, MPI mode needs nr_proc >= 2, Stopping!"
     STOP
  END IF
  !
  ! Get the individual process ID.
  CALL MPI_Comm_rank ( MPI_COMM_WORLD, id_proc, ierr )
  
  !
  ! Command process
  IF ( id_proc .eq. root_process ) THEN
  !
#endif
  
  ! Start code
  CALL welcome(stdo, ver)
  
  ! Get current CPU time
  CALL CPU_TIME(start_cpu_time)
  
  ! TODO: read options from command line
  
  !
  ! Read config files and parameters
  !
  ! Measure task runtime

  CALL CPU_TIME(start_cpu_time_task)
  IF (verbose .ge. 4) WRITE(stdo,64) "PARAMETERS"
  ! Set defaults
  CALL set_param_defaults()
  CALL CPU_TIME(finish_cpu_time)
  ! Main configuration file:
  CALL read_param_file('0io.inp')
  ! Grain properties config
  CALL read_param_file('5surfchem.inp')
  !
  CALL CPU_TIME(finish_cpu_time_task)
  IF (verbose .ge. 4) THEN
     WRITE(stdo,66) finish_cpu_time_task-start_cpu_time_task
     WRITE(stdo,65) "PARAMETERS"
  END IF
#ifdef DEBUG
  !
  ! Initialize debug folder
  CALL check_folder( TRIM(debug_folder) )
#endif
  !
  ! Measure task runtime
  CALL CPU_TIME(start_cpu_time_task)
  IF (verbose .ge. 4) WRITE(stdo,64) "ABUNDANCES" 
  !
  ! Set default initial abundances (Low metals, see comments in config_module.F90
  CALL set_init_abuns_defaults()
  ! Read the initial abundance file
  CALL read_init_abuns('3abunds.inp')
  !
  CALL CPU_TIME(finish_cpu_time_task)
  IF (verbose .ge. 4) THEN
     WRITE(stdo,66) finish_cpu_time_task-start_cpu_time_task
     WRITE(stdo,65) "ABUNDANCES"
  END IF
  !
  ! Read reaction files
  !
  ! Measure task runtime
  CALL CPU_TIME(start_cpu_time_task)
  IF (verbose .ge. 4) WRITE(stdo,64)  "REACTION NETWORK"
  CALL read_species_list(species_file)
  !
  CALL read_reaction_list(reaction_file)
  !
  ! Read rate coefficient fit parameters
  IF (USE_ECOLI) THEN
     CALL read_ecoli_cfit(ecoli_file)
  END IF
  !
  CALL CPU_TIME(finish_cpu_time_task)
  IF (verbose .ge. 4) THEN
     WRITE(stdo,66) finish_cpu_time_task-start_cpu_time_task
     WRITE(stdo,65) "REACTION NETWORK"
  END IF
  !
  ! Read physical conditions
  !
  ! Measure task runtime
  CALL CPU_TIME(start_cpu_time_task)
  IF (verbose .ge. 4) WRITE(stdo,64)  "PHYSICAL CONDITIONS"
  ! Read HDF5 or ascii physical conditions input file
  IF (USE_HDF5) THEN
     ! Read X-ray cross section data if needed
     IF (USE_XRSPEC) THEN
        CALL hdf5_read_xr_sigdb(xrsig_file, xrsig)
     END IF
     IF (USE_UVSPEC) THEN
        CALL hdf5_read_uv_sigdb(uvsig_file, uvsig)
     END IF
     CALL hdf5_read_environ('1environ.hdf5', models, nr_cell)
  ELSE
     CALL read_environ('1environ.inp', models, nr_cell)
  END IF
  !
  CALL CPU_TIME(finish_cpu_time_task)
  IF (verbose .ge. 4) THEN
     WRITE(stdo,66) finish_cpu_time_task-start_cpu_time_task
     WRITE(stdo,65) "PHYSICAL CONDITIONS"
  END IF
  
#ifdef USE_MPI
  ! Finish reading input on root_process
  END IF
  
  !
  ! Broadcat parameters
  CALL bcast_param()
  
  !
  ! Broadcast network
  CALL bcast_network()

  !
  ! root_process: deals with i/o and controlling the workers
  !
  IF ( id_proc .eq. root_process ) THEN
     !
     ! Initialize control variables
     nr_closed_workers = 0
     nr_workers = nr_proc - 1
     task_index = 1
     
     ! Measure task runtime
     CALL CPU_TIME(start_cpu_time_task)
     IF (verbose .ge. 4) WRITE(stdo,64)  "RATES + SOLVE"
     
     !
     ! Wait for ready signal from workers
     DO WHILE ( nr_closed_workers .lt. nr_workers )
        !
        ! Get worker status
        CALL recv_info( message, source )
        !
        ! Send model if worker ready
        IF (message .eq. 0) THEN
           !
           ! Is there still work to do?
           IF (task_index .le. nr_cell) THEN
              !
              ! Send start command
              CALL send_info( 1, root_process, source )
              !
              ! Send task_index
              CALL MPI_SEND( task_index, 1, MPI_INTEGER4, source, data_tag, &
                             MPI_COMM_WORLD, ierr )
              !
              ! Send model
              CALL send_model( models(task_index) , source )
              !
              ! Increase index
              task_index = task_index + 1
           ELSE
              !
              ! Send command to finalize
              CALL send_info( -1, root_process, source )
              !
           END IF
           !
        ELSE IF (message .eq. 2) THEN
           !
           ! Get model task_index back
           CALL MPI_RECV( work_index, 1, MPI_INTEGER4, source,  &
                          data_tag, MPI_COMM_WORLD, status, ierr )
           !
           ! Computation is done, fetch result
           CALL recv_model( models(work_index), source )
           
           ! Check rate calculations
!            print *, models(work_index)%gdens(1)
!            print *, models(work_index)%ak(:,1)
           
           !
        ELSE IF (message .eq. -1) THEN
           !
           ! Worker has nothing to do, close it
           nr_closed_workers = nr_closed_workers + 1
           !
        END IF
        !
    END DO
  
  CALL CPU_TIME(finish_cpu_time_task)
  IF (verbose .ge. 4) THEN
     WRITE(stdo,66) finish_cpu_time_task-start_cpu_time_task
     WRITE(stdo,65) "RATES + SOLVE"
  END IF
  !
  ! worker process: performs rate coefficient and abundance calculations
  !
  ELSE
    !
    DO WHILE (.TRUE.)
       !
       ! Send ready signal
       CALL send_info( 0, id_proc, root_process )
       !
       ! Fetch reply
       CALL recv_info( message, source )
       !
       ! Fetch model if available
       IF (message .eq. 1) THEN
          !
          ! Fetch task_index
          
          CALL MPI_RECV( work_index, 1, MPI_INTEGER4, root_process, data_tag, &
                         MPI_COMM_WORLD, status, ierr )
          !
          ! Fetch current model
          CALL recv_model( worker_model, root_process )
          
          !
          ! Test computing reaction rate coefficients
          !
          CALL tabulate_rate_coef( worker_model )

          !
          ! Test solve chemical reaction network
          !
          CALL drive_solver( worker_model )

          !
          ! Send DONE message
          CALL send_info( 2, id_proc, root_process )
          !
          ! Send back task_index
          CALL MPI_SEND( work_index, 1, MPI_INTEGER4, root_process, data_tag, &
                         MPI_COMM_WORLD, ierr )
          !
          ! Send results
          CALL send_model( worker_model, root_process )
          !
       ELSE IF (message .eq. -1) THEN
          !
          ! Breaking out from loop
          EXIT
          !
       END IF
       !
    END DO
    !
    ! Send finalize message
    CALL send_info( -1, id_proc, root_process )
    !
  END IF
#else

  !
  ! Test computing reaction rate coefficients
  !
  ! Measure task runtime
  CALL CPU_TIME(start_cpu_time_task)
  IF (verbose .ge. 4) WRITE(stdo,64)  "RATE COEFFICIENTS"
  !
  DO i=1, nr_cell
      CALL tabulate_rate_coef( models(i) )
  END DO
  !
  CALL CPU_TIME(finish_cpu_time_task)
  IF (verbose .ge. 4) THEN
     WRITE(stdo,66) finish_cpu_time_task-start_cpu_time_task
     WRITE(stdo,65) "RATE COEFFICIENTS"
  END IF
  
  !
  ! Test solve chemical reaction network
  !
  ! Measure task runtime
  CALL CPU_TIME(start_cpu_time_task)
  IF (verbose .ge. 4) WRITE(stdo,64)  "SOLVE NETWORK"
  DO i=1, nr_cell  
      CALL drive_solver( models(i) )
  END DO
  !
  CALL CPU_TIME(finish_cpu_time_task)
  IF (verbose .ge. 4) THEN
     WRITE(stdo,66) finish_cpu_time_task-start_cpu_time_task
     WRITE(stdo,65) "SOLVE NETWORK"
  END IF
  
#endif
  
#ifdef USE_MPI
  !
  ! root_process deals with writting out
  IF ( id_proc == root_process ) THEN
#endif
  !
  ! Write results to file(s)
  ! Check/create results folder
  !
  
  !
  ! Write IDL and python readable detailed output
  !
  ! Measure task runtime
  CALL CPU_TIME(start_cpu_time_task)
  IF (verbose .ge. 4) WRITE(stdo,64)  "IDL OUTPUT"
  !
  IF ( OIDL ) CALL write_idl_out(result_folder, out_base_name, models, nr_cell)
  !
  CALL CPU_TIME(finish_cpu_time_task)
  IF (verbose .ge. 4) THEN
     WRITE(stdo,66) finish_cpu_time_task-start_cpu_time_task
     WRITE(stdo,65) "IDL OUTPUT"
  END IF
  !
  ! Write quick access Binary output (readable from IDL or python)
  ! (Note it contains less data compared to the IDL output, 
  ! however, binary output is prefered for large models.)
  !
  CALL CPU_TIME(start_cpu_time_task)
  IF (verbose .ge. 4) WRITE(stdo,64)  "BINARY OUTPUT"
  !
  IF ( OBIN ) CALL write_bin_out(result_folder, out_base_name, models, nr_cell)
  !
  CALL CPU_TIME(finish_cpu_time_task)
  IF (verbose .ge. 4) THEN
     WRITE(stdo,66) finish_cpu_time_task-start_cpu_time_task
     WRITE(stdo,65) "BINARY OUTPUT"
  END IF
  !
  ! Write Human readable plain text output file
  ! Note: plain text output is *very* slow, use only when absolutely necessary
  !
  CALL CPU_TIME(start_cpu_time_task)
  IF (verbose .ge. 4) WRITE(stdo,64)  "PLAIN TEXT OUTPUT"
  !
  IF ( ODAT ) CALL write_ascii_out(result_folder, out_base_name, models, nr_cell)
  !
  CALL CPU_TIME(finish_cpu_time_task)
  IF (verbose .ge. 4) THEN
     WRITE(stdo,66) finish_cpu_time_task-start_cpu_time_task
     WRITE(stdo,65) "PLAIN TEXT OUTPUT"
  END IF
  !
  ! Write HDF5 binary output file
  !
  CALL CPU_TIME(start_cpu_time_task)
  IF (verbose .ge. 4) WRITE(stdo,64)  "PLAIN TEXT OUTPUT"
  !
  IF ( OHDF ) CALL write_hdf5_out(result_folder, out_base_name, models, nr_cell)
  !
  CALL CPU_TIME(finish_cpu_time_task)
  IF (verbose .ge. 4) THEN
     WRITE(stdo,66) finish_cpu_time_task-start_cpu_time_task
     WRITE(stdo,65) "HDF5 OUTPUT"
  END IF
  !
  !
  ! Get the current CPU time and calculate the elapsed times
  CALL CPU_TIME(finish_cpu_time)
  WRITE(stdo,67) finish_cpu_time-start_cpu_time
  
  !
#ifdef USE_MPI
  !
  ! Finish writing output on root_process
  END IF
  !
  ! Terminate MPI.
  CALL MPI_Finalize ( ierr )
#endif

END PROGRAM mpchem
