#!/usr/bin/perl
#=======================================================
#           INSTALL ROUTINE FOR "ALCHEMIC"
# The install routine is based on the Cornelis Dullemond's
# RADMC-3D installer
#=======================================================
$pwd  = `pwd` ;
chop($pwd) ;
print "Installing MPChem in the directory:\n  '$pwd'\n";
$home = $ENV{"HOME"};
$bin  = $home . "/bin" ;
if(!(-e $bin)) {
    print "You must have a bin/ directory in your home directory\n" ;
    print "-----Shall I make $bin for you?\n" ;
    $input = <STDIN> ;
    print $input ;
    if($input=~/^[yY]/) {
	print "Creating $bin for you...\n" ;
	system("mkdir $bin") ;
    }
}
$path = $ENV{"PATH"};
if(!($path =~ /$bin/)) {
    print "The $bin directory exists, but it not in the PATH environment variable\n" ;
    print "You must put the \n" ;
    print "$bin directory \n" ;
    print "in the path yourself (in the .tcshrc file if you use the tcsh shell...)\n" ;
    print "If you do it now, don't forget to type 'rehash'.\n" ;
}
print "  Creating a link 'mpchem' in '$bin/'\n" ;
$mpchem    = $pwd . "/mpchem" ;
$mpchemlnk = $bin . "/mpchem" ;
if(!(-e $mpchemlnk)) {
    print "------ Warning: file $mpchemlnk did not exist previously. You might want to type 'rehash'\n" ;
}
open(FILE,">$mpchemlnk") || die "Could not open file\n" ;
print FILE "#!/usr/bin/perl\n" ;
print FILE "system(\"$mpchem \@ARGV\");" ;
close (FILE) ;
`chmod u+rwx $mpchemlnk` ;
#
# The following is only necessary if you want to use the IDL analysis tools.
# You can remove or comment-out the following lines if you do not want to
# use these tools.
#
$idl  = $home . "/bin/idl" ;
if(!(-e $idl)) {
    print "It is convenient for you to have a bin/idl/ directory in your home directory\n" ;
    print "-----Shall I make $idl for you?\n" ;
    $input = <STDIN> ;
    print $input ;
    if($input=~/^[yY]/) {
	print "Creating $idl for you...\n" ;
	system("mkdir $idl") ;
    }
}
if(-e $idl) {
    print "Found the ~/bin/idl/ directory. Copying current idl analysis tools there.\n" ;
    system("cp ../idl_scripts/read_alchemic.pro $idl/") ;
}
