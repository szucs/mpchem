#
# This will compile and test the MPChem code
#
import os
import subprocess
import sys
sys.path.append('python')
import pychemic as pych
import numpy as np
import warnings

def run_command(command):
    p = subprocess.Popen(command,
                         stdout=subprocess.PIPE,
                         stderr=subprocess.STDOUT)
    out = p.stdout.readlines()
    return out[:]

def compile_mpchem():
    '''
    #Clean source directory and compile code
    '''
    current_dir = os.path.realpath('.')
    work_path = os.path.realpath('./src')
    os.chdir(work_path)
    print ("INFO: Build directory: {}".format(work_path))
    errors = 0
    try:
        stdout0 = run_command("make clean".split())
    except:
        stdout0 = ['ERROR: make clean failed']
        errors = errors + 1
    try:
        stdout1 = run_command("make")
    except:
        stdout0 = ['ERROR: make failed']
        errors = errors + 1
    for line in stdout0:
        print (line)#(line.replace("\n",""))
    for line in stdout1:
        print (line) #.replace("\n",""))
    os.chdir(current_dir)
    return errors

def install_mpchem():
    '''
    Install code to ~/bin
    '''
    current_dir = os.path.realpath('.')
    work_path = os.path.realpath('./src')
    os.chdir(work_path)
    print ("INFO: Build directory: {}".format(work_path))
    errors = 0
    try:
        stdout0 = run_command("make install".split())
    except:
        stdout0 = ['ERROR: make install failed']
        errors = errors + 1
    for line in stdout0:
        print (line)#.replace("\n",""))
    os.chdir(current_dir)
    return errors

def kida_model(epsilon=1e-1,work_dir='test/kida.uva.2014',
               spec=['ELECTR','NO','HC3N','CCS','l-C3H2','CH3OH']):
    '''
    Test code with the KIDA network.

    The model is computed with the Nautilus code and the 2014 release of the
    Kinetic Database for Astrochemistry. See the README in the test model
    folder for more information.
    The default tested species include ELECTR, NO, HC3N, CCS, l-C3H2 and
    CH3OH. The choices are motivated by Fig. 1 in the KIDA 2014 release paper.

    Test model reference: Wakelam et al. 2015, ApJS, 217, 20

    Parameters:

    :param epsilon: Tolerated relative difference to the reference abundances.
                    Default: 0.1
    :param test_dir: Subdirectory of test model relative to the main code dir.
    :param spec: List of species to be tested.
    '''
    current_dir = os.path.realpath('.')
    # Change directory to the KIDA test folder
    os.chdir(work_dir)
    # Check if the location is right:
    dir_path = os.path.realpath('.')
    print ("Build directory: {}".format(dir_path))
    # Set the error counter
    errors = 0
    # We should run the MPChem model!
    try:
        stdout0 = run_command("mpchem".split())
    except:
        stdout0 = ['ERROR: KIDA model failed']
        errors = errors + 1
    if errors > 0:
        for line in stdout0:
            print (line)#.replace("\n",""))
        return 1
    # Read the KIDA/Nahoon reference models
    w15 = pych.model.read_nahoon('nautilus-models/nahoon_2e4cm-3_10K_30mag.dat',ns=490)
    # Read the MPChem model
    tmc1 = pych.model.read_alchemic("results/kida2014_00000001.idl")
    # Return to code main directory
    os.chdir(current_dir)
    # Compare to reference
    errors = compare_models(w15, tmc1, spec, epsilon=epsilon, time=w15.time[1:])

    return errors

def semenov_model(epsilon=1e-1,work_dir='test/semenov2010',
               spec=['ELECTR','C+','CO','NH3','H2O','CH4O','H2S']):
    '''
    Test code with the Semenov et al. (2010) astrochemical code benchmark.

    The test models are computed with the ALCHEMIC code and include dark
    core, hot core and protoplanetary disk conditions. The chemical network
    includes grain surface species and reactions. See the README in the test
    model folder for more information.
    The default tested species include ELECTR, C+, NH3, gH2O, C6H6 and
    CH3OH. The choices are motivated by Fig. 1 in the benchmark paper.

    Benchmark reference: Semenov et al. 2010, A&A, 522, 42

    Parameters:

    :param epsilon: Tolerated relative difference to the reference abundances.
                    Default: 0.1
    :param test_dir: Subdirectory of test model relative to the main code dir.
    :param spec: List of species to be tested.
    '''
    current_dir = os.path.realpath('.')
    # Change directory to the Sipila et al. (2016) test folder
    os.chdir(work_dir)
    # Check if the location is right:
    dir_path = os.path.realpath('.')
    print ("Build directory: {}".format(dir_path))
    # Set the error counter
    errors = 0
    # We should run the ALCHEMIC model!
    try:
        stdout0 = run_command("mpchem".split())
    except:
        stdout0 = ['ERROR: Semenov2010 model failed']
        errors = errors + 1
    if errors > 0:
        for line in stdout0:
            print (line.replace("\n",""))
        return 1
    # Read the Benchmark reference models
    tmc1 = pych.model.read_semenov2010('semenov2010_benchmark/TMC1.osu0803.full.2d_4cm_3.10K.1.3d_17s_1.1G0.low_metals_1.idl')
    hotcore = pych.model.read_semenov2010('semenov2010_benchmark/HOT_CORE.osu0803.full.2d_7cm_3.100K.1.3d_17s_1.1G0.low_metals_1.idl')
    disk1 = pych.model.read_semenov2010('semenov2010_benchmark/DISK.osu0803.full.low_metals_1.idl')
    disk2 = pych.model.read_semenov2010('semenov2010_benchmark/DISK.osu0803.full.low_metals_2.idl')
    disk3 = pych.model.read_semenov2010('semenov2010_benchmark/DISK.osu0803.full.low_metals_3.idl')
    # Combine models to list
    benc_data = [tmc1, hotcore, disk1, disk2, disk3]
    # Read MPChem models
    mpc_data = pych.model.read_alchemic('results/semenov2010.hdf5')
    os.chdir(current_dir)

    # Compare models
    for i in range(len(benc_data)):
        errors = errors + compare_models(benc_data[i], mpc_data[i], spec,
                                         epsilon=epsilon, time=
                                         np.array([1e3,1e4,1e5,1e6]))
    return errors

def compare_models(m1, m2, spec, epsilon=1e-1, time=None):
    '''
    Compares abundances in chemical networks m1 and m2. Both of these should
    be a pychemic.ChemModl object. The routine returns 0 if models match
    within tolerance epsilon and otherwise an integer larger than 0.
    The optional time argument sets an array of times (in year) when the
    comparison is made. If not set, then the times from m2 are used.

    Parameters:
    :param m1, m2: pychemic.ChemModl objects to be compared.
    :param spec: List of chemical species names. These species are compared.
    :param epsilon: Tolerance of relative difference, default is 0.1.
    :param time: optional numpy.ndarray setting times (in year) at which models
                 are compared.
    '''
    # Determine model time steps
    t1 = m1.time
    t2 = m2.time
    if type(time) != np.ndarray:
        time = t2
    # Set up variables
    diff = np.zeros(len(spec))
    errors = 0
    # Loop over the species
    for i in range(len(spec)):
        iM1 = m1.spec.index(spec[i])
        iM2 = m2.spec.index(spec[i])
        M1Interp = np.interp(time, t1, m1.abuns[iM1,:])
        M2Interp = np.interp(time, t2, m2.abuns[iM2,:])
        diff[i] = max(abs(M2Interp - M1Interp) / M1Interp)
        if diff[i] > epsilon:
            warnings.warn("SPECIES (absolute difference): {:s} ({:.2E})".format(spec[i], diff[i]))
            errors = errors + 1
    # Return
    return errors


import pytest

def test_compile():
    assert compile_mpchem() == 0

def test_install():
    assert install_mpchem() == 0

def test_kida():
    assert kida_model(epsilon=1.5e-1) == 0

#def test_semenov():
    #assert semenov_model(epsilon=5e-1) <= 5
