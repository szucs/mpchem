#
# CI Regression tests for MPChem code
#
import os
import subprocess
import pychemic as pych
import numpy as np
import warnings
import pytest

def run_command(command):
    p = subprocess.Popen(command,
                         stdout=subprocess.PIPE,
                         stderr=subprocess.STDOUT)
    out = p.stdout.readlines()
    return out[:]

def base(result, regression, epsilon=1e-6, work_dir='test/kida.uva.2014', 
         bin_path='src/', spec=['ELECTR','NO','HC3N','CCS','l-C3H2','CH3OH']):
    '''
    Base test function, all tests call this function.

    Parameters:
    :param result: Path to model results (string)
    :param regression: Path to regression results (string)
    :param epsilon: Tolerated relative difference to the reference abundances.
                    Default: 1.0E-6
    :param work_dir: Subdirectory of test model relative to the main code dir.
    :param bin_path: Relative path to MPChem binary
    :param spec: List of species to be tested.
    '''
    current_dir = os.path.realpath('.')
    bin_path = f'{current_dir}/{bin_path}'
    # Change directory to the KIDA test folder
    os.chdir(work_dir)
    # Check if the location is right:
    dir_path = os.path.realpath('.')
    print (f'Build directory: {dir_path}')
    # Set the error counter
    errors = 0
    # We should run the MPChem model!
    try:
        stdout0 = run_command(f'{bin_path}/mpchem'.split())
    except:
        stdout0 = ['ERROR: model run failed', f'{bin_path}/mpchem']
        errors = errors + 1
    if errors > 0:
        for line in stdout0:
            print (line)#.replace("\n",""))
        return 1
    # Read the reference models
    reg = pych.model.read_alchemic(regression)
    # Read the MPChem model
    tmc1 = pych.model.read_alchemic(result)
    # Return to code main directory
    os.chdir(current_dir)
    # Compare to reference
    if len(reg) != len(tmc1):
        return 1
    for i in range(len(reg)):
        errors =+ compare_models(reg[i], tmc1[i], spec, epsilon=epsilon, time=reg[i].time[1:])

    return errors

def compare_models(m1, m2, spec, epsilon=1e-1, time=None):
    '''
    Compares abundances in chemical networks m1 and m2. Both of these should
    be a pychemic.ChemModl object. The routine returns 0 if models match
    within tolerance epsilon and otherwise an integer larger than 0.
    The optional time argument sets an array of times (in year) when the
    comparison is made. If not set, then the times from m2 are used.

    Parameters:
    :param m1, m2: pychemic.ChemModl objects to be compared.
    :param spec: List of chemical species names. These species are compared.
    :param epsilon: Tolerance of relative difference, default is 0.1.
    :param time: optional numpy.ndarray setting times (in year) at which models
                 are compared.
    '''
    # Determine model time steps
    t1 = m1.time
    t2 = m2.time
    if type(time) != np.ndarray:
        time = t2
    # Set up variables
    diff = np.zeros(len(spec))
    errors = 0
    # Loop over the species
    for i in range(len(spec)):
        iM1 = m1.spec.index(spec[i])
        iM2 = m2.spec.index(spec[i])
        M1Interp = np.interp(time, t1, m1.abuns[iM1,:])
        M2Interp = np.interp(time, t2, m2.abuns[iM2,:])
        diff[i] = max(abs(M2Interp - M1Interp) / M1Interp)
        if diff[i] > epsilon:
            warnings.warn("SPECIES (relative difference): {:s} ({:.2E})".format(spec[i], diff[i]))
            errors = errors + 1
    # Return
    return errors

def test_kida():
    '''
    Test code with the KIDA network.

    Test model reference: Wakelam et al. 2015, ApJS, 217, 20
    '''
    assert 'MPCHEM_REG_HOME' in os.environ
    
    kida_dir = 'test/kida.uva.2014'
    kida_spec = ['ELECTR','NO','HC3N','CCS','l-C3H2','CH3OH']
    kida_result = 'results/kida2014.hdf5' # relative
    kida_regression = '{}/kida2014.hdf5'.format(os.environ['MPCHEM_REG_HOME'])
    
    assert base(kida_result, kida_regression, work_dir=kida_dir, spec=kida_spec,
                epsilon=1.0e-3) == 0
    
def test_dynamic():
    '''
    Test code with streamline data (time dependent physical conditions)
    '''
    assert 'MPCHEM_REG_HOME' in os.environ
    
    dynamic_dir = 'test/sph-trajectory/kida_network'
    dynamic_spec = ['ELECTR','NO','HC3N','CCS','l-C3H2','CH3OH']
    dynamic_result = 'results/kida2014.hdf5' # relative
    dynamic_regression = '{}/sph-trajectory.hdf5'.format(os.environ['MPCHEM_REG_HOME'])
    
    assert base(dynamic_result, dynamic_regression, work_dir=dynamic_dir, 
                spec=dynamic_spec, epsilon=1.0e-6) == 0
    
def test_semenov():
    '''
    Test code with the Semenov et al. (2010) astrochemical code benchmark.

    Benchmark reference: Semenov et al. 2010, A&A, 522, 42
    '''
    assert 'MPCHEM_REG_HOME' in os.environ
    
    semenov_dir = 'test/semenov2010'
    semenov_spec = ['ELECTR','C+','CO','NH3']
    semenov_result = 'results/semenov2010.hdf5' # relative 
    semenov_regression = '{}/semenov2010.hdf5'.format(os.environ['MPCHEM_REG_HOME'])
    
    assert base(semenov_result, semenov_regression, work_dir=semenov_dir, 
                spec=semenov_spec, epsilon=1.0e-4) == 0
