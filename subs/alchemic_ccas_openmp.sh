#$ -S /bin/bash
#$ -j y
#$ -cwd
#$ -m n
#$ -N HW1-alchemic
#$ -t 1-24
#$ -l h_vmem=2G
#$ -l h_rt=7200
#$ -pe openmp 1

date
which alchemic

module load gcc/5.4
module load mkl

cd /u/szucs/postproc/hw1
cd particles_$SGE_TASK_ID

echo "Starting thread:" $SGE_TASK_ID
pwd

alchemic 1 1
