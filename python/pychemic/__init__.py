"""
pychemic: ALCHEMIC Python module
Laszlo Szucs, Garching, 2016
"""
from al_model import read_alchemic
from al_model import read_sipila2015
from al_model import read_semenov2010
from al_model import read_nahoon
from al_model import write_alchemic
from al_network import read_network
from al_network import add_grain
from al_gadget import read_gadget
from al_gadget import write_asplash
from al_gadget import read_history_dump
from al_model import plot_rad_dist
from al_model import write_nautilus
from al_model import read_nautilus

contents = ["read_alchemic","read_sipila2015","read_semenov2010","read_network",
           "add_grain","read_nahoon","read_gadget","write_asplash",
           "write_alchemic","read_history_dump","plot_rad_dist","write_nautilus",
           "read_nautilus"]

# Check if pygraphviz is installed
try:
    import pygraphviz
    contents.append("draw_chem_graph")
    from al_chemgraph import draw_chem_graph
except ImportError, e:
    pass

__version__ = "0.1"
__all__ =  contents #,"draw_chem_graph"]
