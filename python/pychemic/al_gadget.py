from scipy.io import readsav
import numpy as np
import al_commons
import al_model
import os
import glob

def read_gadget(filename="hw_cloud_1.2box.sav",idSelect=215090l,verbose=False,
            allpart=False):
    ''' Reads IDL save files created from GADGET simulation snapshots by selecting
    particles and following their time evolution (physical conditions, shielding
    and abundances).

    The read data is converted to PyChemic ChemModl class.

    This code is still under developement! For now it works with a single SPH
    particle.
    '''
    mp = 1.6726000e-24    # proton mass in grams
    year = 31557600.0     # year in seconds

    PartData = readsav(filename,verbose=verbose).tracedpart # this is np.recarray
    IDs = PartData.ID.tolist()

    # Check whether idSelect is an array/list or a single number
    if allpart == False:
        if (type(idSelect) != list) and (type(idSelect) != np.ndarray):
            idSelect = [idSelect]
    else:
        idSelect = IDs
    models = []

    for i in range(len(idSelect)):
        try:
            loc = IDs.index(idSelect[i])
            ID = IDs[loc]
            time = np.array(PartData.TIME.tolist()[loc])
            time = time / year
            rho = np.array(PartData.RHO.tolist()[loc])
            gdens = rho / (1.4 * mp)
            Tg = np.array(PartData.TGAS.tolist()[loc])
            Td = np.array(PartData.TDUST.tolist()[loc])
            fH2_IS = np.array(PartData.H2SHIELDFACTOR.tolist()[loc])
            fCO_IS = np.array(PartData.COSHIELDFACTOR.tolist()[loc])
            pos = PartData.POS.tolist()[loc]
            if len(time) > 1:
                x = np.array(pos[:,0])
                y = np.array(pos[:,1])
                z = np.array(pos[:,2])
            else:
                x = np.array(pos[0])
                y = np.array(pos[1])
                z = np.array(pos[2])
            abuns = np.array(PartData.CHEM.tolist()[loc])
            spec = PartData.SPECIES.tolist()[loc]
            AvArr = np.array(PartData.AV.tolist()[loc])
            AvEff = np.array(PartData.AVEFF.tolist()[loc])
            H = np.array(PartData.H.tolist()[loc])
	# Filter out timesteps after particle accretion to sink
	    ba = np.array(gdens.nonzero())[0,:]
            args = {'ID': ID,'x':x[ba], 'y':y[ba], 'z':z[ba], 'time':time[ba], 'Tg':Tg[ba], 'Td':Td[ba],
                'rho':rho[ba],'AvIS':AvEff[ba], 'AvArr':AvArr[ba,:], 'G0':1.7, 'ZetaCR':2E-17, 'gdens':gdens[ba],
                'amu':1.4, 'spec':spec.tolist(), 'abuns':abuns[ba,:].transpose(),
                'fH2_IS':fH2_IS[ba], 'fCO_IS':fCO_IS[ba], 'H':H[ba]}
            models.append(al_model.ChemModl(**args))
        except:
            print idSelect[i], "not found in", filename

    PartData = None                 # free up memory -- hopefully
    if len(idSelect) == 1:       # if only 1 particle return that
        models = models[0]
    return models

def write_asplash(ModelList=None,filename='asplash.dat',CordRenorm=False,
            snapshot=-1,Speclist=["H2","CO","H2O","N2H+","NH3","HCN","HNC",
            "CH3OH"],verbose=False,savname="hw_cloud_1.2box.sav",savdata=None,
            mode="w"):
    ''' Writes a list of ChemModl models to a Splash (Price 2009) ascii file
    to be then interpolated to a regular grid. For this additional data is
    needed besides the chemical information: location (x,y,z), smoothing length
    (h), and ideally velocity (vx,vy,vz).
    '''
    # do tests first:
    nCell = len(ModelList)
    time = ModelList[0].time
    if (ModelList == None) or (nCell == 1):
        raise ValueError("A list of ChemModl object needs to be given as input!")
    # Set the conversion factors to Gadget internal units
    ulength = 1.0E17
    umass = 1.991E33
    uvel = 36447.3E0
    utime = 2.74368E12
    udens = 1.991E-18 #umass / ulength^3.
    # Renormalise coordinates if asked
    xCent = 0.0E0
    yCent = 0.0E0
    zCent = 0.0E0
    if CordRenorm:
        PartDens = np.zeros(nCell)
        for i in range(nCell):
            PartDens[i] = ModelList[i].gdens[snapshot]
        IndRhoMax = PartDens.argmax()
        xCent = ModelList[IndRhoMax].x[snapshot]
        yCent = ModelList[IndRhoMax].y[snapshot]
        zCent = ModelList[IndRhoMax].z[snapshot]
    # Find the index of selected species
    SpecIndex = []
    for ss in Speclist:
        try:
            tmpInd = ModelList[0].spec.index(ss)
            SpecIndex.append(tmpInd)
        except:
            print ss, "not found in the input chemical models, skiping it!"
            SpecIndex.remove(ss)
    # get addidional information about the particles (vx,vy,vz,h)
    if type(savdata) != np.recarray:
        if not os.path.isfile(savname):
            raise ValueError(savname+" does not exist, but needed to proceed!")
        # continue if found
        print("Reading Gadget IDL save from '%s'..." % savname),
        PartData = readsav(savname,verbose=verbose).tracedpart # this is np.recarray
        print("Done")
    else:
        PartData = savdata
    IDL_IDs = [int(i) for i in PartData.ID.tolist()]
    IDL_time = PartData.TIME[0]  # time from the first SPH particle, should be the same for all
    IDL_pos = PartData.POS.tolist()
    IDL_vel = PartData.vel.tolist()
    IDL_rho = PartData.rho.tolist()
    IDL_h = PartData.H.tolist()
    IDL_mass = PartData.MASS.tolist()
    #now write the ascii file!
    # check if output file exists:
    FileExists = os.path.isfile(filename)
    if FileExists and (mode == "a"):
        fdat = open(filename, mode)
        fcol = open('columns', "w")
    else:
        fdat = open(filename, "w")
        fcol = open('columns', "w")
        fdat.write('{:11.4E}\n'.format(time[snapshot]))
        fdat.write('{:8d}  {:8d}  {:8d}  {:8d}  {:8d}  {:8d}\n'.format(nCell,0,0,0,0,0))
    for i in range(nCell):
        # find the current particle in the Gadget IDL save data
        IDL_loc = IDL_IDs.index(ModelList[i].ID)
        pos = IDL_pos[IDL_loc][(snapshot-1)]
        vel = IDL_vel[IDL_loc][(snapshot-1)]
        h = IDL_h[IDL_loc][(snapshot-1)]
        mass = IDL_mass[IDL_loc][(snapshot-1)]
        rho = IDL_rho[IDL_loc][(snapshot-1)]
        # write the location
        fdat.write( '{:11.4E}  {:11.4E}  {:11.4E}  '.format(   # x, y, z
            (pos[0]-xCent)/ulength,
            (pos[1]-yCent)/ulength,
            (pos[2]-zCent)/ulength ) )
        if not fcol.closed: fcol.write('x\ny\nz\n')
        # write the velocities
        fdat.write( '{:11.4E}  {:11.4E}  {:11.4E}  '.format(   # x, y, z
            (vel[0]-xCent)/uvel,
            (vel[1]-yCent)/uvel,
            (vel[2]-zCent)/uvel ) )
        if not fcol.closed: fcol.write("vx\nvy\nvz\n")
        # write the particle mass, density and temperatures
        fdat.write( '{:11.4E}  '.format(
            mass/umass ))         # particle mass
        if not fcol.closed: fcol.write('particle mass\n')
        fdat.write( '{:11.4E}  {:11.4E}  {:11.4E}  '.format(
            ModelList[i].rho[snapshot]/udens,   # density
            ModelList[i].Tg[snapshot],          # gas temperature
            ModelList[i].Td[snapshot] ))         # dust temperature
        if not fcol.closed: fcol.write('density\nTgas\nTdust\n')
        # write the smoothing length
        fdat.write( '{:11.4E}  '.format(
            h/ulength ))         # smoothing length
        if not fcol.closed: fcol.write('h\n')
        # finally, write the chemical species * density (i.e. number density)
        for jj in range(len(SpecIndex)):
            fdat.write( '{:11.4E}  '.format(
                ModelList[i].abuns[SpecIndex[jj],snapshot]*
                ModelList[i].gdens[snapshot]) )
            if not fcol.closed: fcol.write(Speclist[jj]+"\n")
        fdat.write("\n")            # get to the next particle
        if not fcol.closed: fcol.close()                # all columns are set, close the file

    fdat.close()
    # free up memory
    del IDL_IDs, IDL_time, IDL_pos, IDL_vel, IDL_rho, IDL_h, IDL_mass, ModelList

def AvgAv(array, npix=48, gamma=-2.5):
    ''' Calculates the effective visual exintction from an array of
    HEALPIX extinction values.
    '''
    # Check if array has npix number of elements
    if len(array) == npix:
        AvMean = np.log((np.exp(gamma*array)).sum() / float(npix)) / gamma
    else:
        print "Error in AvgAv(): ", len(array), "!=", npix
        AvMean = None
    return AvMean

def read_history_dump(folder, idlistFile='id-list.txt',nl99=None):
    # Physical constants
    year = 31557600.
    amu = 1.4
    mp = 1.6726000e-24       # proton mass in grams
    HtotAv = 5.347879e-22    # Total column density to Av conversion factor
    # Code units in cgs
    ct = 2.743687e12      # time -> code unit to s
    cdens = 1.991e-18     # gram
    clength = 1e17        # cm
    # Change to specified folder if given
    if folder:
        try:
            os.chdir(folder)
        except:
            print "Can not open specified folder!"
    # Try to find the ID list first
    if os.path.isfile(idlistFile) == False:
        print idlist + " does not exists in current folder!"
        return -1
    # Look for the history dumps
    dump_list = sorted(glob.glob('history.?')) + sorted(glob.glob('history.??'))
    if len(dump_list) == 0:
        print "No dump files founds in current folder!"
        return -2
    # Open the ID list file
    fid = open(idlistFile, 'r')
    NPart = int(fid.readline())
    idList = []
    # Create nested list to store and dynamically extend the particles data
    time = []
    gdens = []
    Tg = []
    Td = []
    # for not don't consider more species
    xH2 = []
    xHp = []
    xCp = []
    xCHx = []
    xOHx = []
    xCO = []
    xHCOp = []
    xHEp = []
    xMp = []
    x13CO = []
    x13Cp = []
    x13CHx = []
    xH13COp = []
    AvArr = []
    AvEff = []
    for i in range(NPart):
        idList.append(int(fid.readline()))
        time.append([])
        gdens.append([])
        Tg.append([])
        Td.append([])
        xH2.append([])
        xHp.append([])
        xCp.append([])
        xCHx.append([])
        xOHx.append([])
        xCO.append([])
        xHCOp.append([])
        xHEp.append([])
        xMp.append([])
        x13CO.append([])
        x13Cp.append([])
        x13CHx.append([])
        xH13COp.append([])
        AvArr.append([])
        AvEff.append([])
    fid.close()
    # Now read the dump files
    for dump in dump_list:
        fd = open(dump,'r')
        for line in fd:
            try:
                elements = line.split()
                index = idList.index(int(elements[0]))
                time[index].append(float(elements[1]) * ct / year)    # in year
                gdens[index].append(float(elements[2]) * cdens / (amu*mp))
                Tg[index].append(float(elements[3]))
                Td[index].append(float(elements[4]))
                xH2[index].append(float(elements[5]))
                xHp[index].append(float(elements[6]))
                xCp[index].append(float(elements[7]))
                xCHx[index].append(float(elements[8]))
                xOHx[index].append(float(elements[9]))
                xCO[index].append(float(elements[10]))
                xHCOp[index].append(float(elements[11]))
                xHEp[index].append(float(elements[12]))
                xMp[index].append(float(elements[13]))
                x13CO[index].append(float(elements[14]))
                x13CHx[index].append(float(elements[15]))
                x13Cp[index].append(float(elements[16]))
                xH13COp[index].append(float(elements[17]))
                Avtmp = (np.array(elements[18:66],dtype=np.float)
                    / (amu*mp) * HtotAv * cdens * clength)
                AvArr[index].append(Avtmp)
                AvEff[index].append(AvgAv(Avtmp))
            except:
                pass
        fd.close()
    # Store the data in ChemModl class object:
    models = []
    species = ['H2','H+','C+','CHx','OHx','CO','HCO+','HE+','M+',"13CO",
               '13CHx','13C+','H13CO+']
    for i in range(NPart):
        sindex = sorted(range(len(time[i])), key=lambda k: (time[i])[k])
        abuns = np.array([xH2[i],xHp[i],xCp[i],xCHx[i],
                xOHx[i],xCO[i],xHCOp[i],xHEp[i],xMp[i],
                x13CO[i],x13CHx[i],x13Cp[i],xH13COp[i]])
        # sort according time
        try:
            for j in range(len(abuns[:,0])-1):
                abuns[j,:] = abuns[j,:][sindex]
        except:
            pass
        AvLocal = 0.0
        if nl99:
            try:
                for m in range(len(nl99)):
                    if nl99[m].ID == idList[i]:
                        fH2_IS = np.interp(np.array(time[i])[sindex],nl99[m].time,nl99[m].fH2_IS)
                        fCO_IS = np.interp(np.array(time[i])[sindex],nl99[m].time,nl99[m].fCO_IS)
                        H = np.interp(np.array(time[i])[sindex],nl99[m].time,nl99[m].H) # in cgs
                        AvLocal = H * np.array(gdens[i])[sindex] * HtotAv
                        break
            except:
                fH2_IS = None
                fCO_IS = None
                H = None
                AvLocal = None
        args = {'ID': idList[i],'x':None, 'y':None, 'z':None,
            'time':np.array(time[i])[sindex],'Tg':np.array(Tg[i])[sindex],
            'Td':np.array(Td[i])[sindex],'rho':np.array(gdens[i])[sindex]*amu*mp,
            'G0':1.7,'ZetaCR':2E-17,'gdens':np.array(gdens[i])[sindex],
            'amu':amu,'spec':species,'AvIS':np.array(AvEff[i])[sindex],
            'abuns':abuns,'AvArr':np.array(AvArr[i])[sindex,:],'fH2_IS':fH2_IS,
            'fCO_IS':fCO_IS, 'H': H, 'AvLocal':AvLocal}
        models.append(al_model.ChemModl(**args))
    return models
