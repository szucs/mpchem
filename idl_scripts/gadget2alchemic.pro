@co-shield.pro
;
; PROCEDURE GADGET2ALCHEMIC
;
; This procedure converts the idl structure read from the GADGET SPH snapshots
; to IDL structure to ALCHEMIC input ascii format. It takes output from tracedpart.pro
; and writes out the selected particles (idarray) to the selected filename (envname).
; The input database could come from snapshots or the more frequent dump files (the
; later most likely doesn't work in this version).
;
; In this version we write out the visual extinction Av in the 48 traced healpix
; directions. (this makes a big difference in the photo-chemistry)
;
; If you want to write out the whole IDL database, then read the IDs from the
; database and give it as the idarray argument of this procedure.
;
PRO gadget2alchemic, database, envname=envname, dbtype=dbtype, idarray=idarray,$
          allAvs=allAvs, final=final
  IF NOT KEYWORD_SET(form) then form = 1
  year = 3.1556952E7
  RESTORE, database

  IF NOT KEYWORD_SET(dbtype) THEN dbtype=1       ; 1 if snapshot, 0 if dump
  IF (dbtype = 1) THEN array = tracedpart
  IF (dbtype = 0) THEN array = hist
  IF (dbtype ne 1) AND (dbtype ne 0) THEN BEGIN
    PRINT, 'Invailed database type!' & STOP
  ENDIF
  IF NOT KEYWORD_SET(envname) THEN envname = '1environ_0d.inp'
  IF NOT KEYWORD_SET(idarray) THEN idarray = [1035326UL]

  CRP = 2.00e-17    ; Cosmic ray ionisation rate of H2
                    ; This is the equivalent for H_CRP = 1E-17
  OPENW, 01, envname
  pn = 0                                                   ; number of particles to write out
; WRITE 1environ_0d.inp HEADER
  PRINTF, 01, '# !Dynamic! ID x y z rho Tg  Td  G0  AvSt  AvIS  ZetaCR  ZetaX fH2_St  fCO_St fH2_IS  fCO_IS T00 T01 Hr  T0  TF'
  PRINTF, 01, FORMAT = '(1(I4))', [N_ELEMENTS(idarray)]
  PRINTF, 01, FORMAT = '(1(I4))', [1]
  FOR i=0, N_ELEMENTS(array.id)-1 DO BEGIN
     IF (WHERE(array[i].id EQ idarray) NE -1) THEN BEGIN
        PRINT, 'Writing data of particle: ', STRCOMPRESS(STRING(array[i].ID))
        Before_Accretion = n_elements(where(array[i].time gt 0. ))
        IF keyword_set(final) THEN BEGIN
        	Before_Accretion = min([Before_Accretion,final])
        ENDIF
        PRINTF, 01, FORMAT = '(I4, TR2, I4)', [Before_Accretion-1, 0]
; WRITE PHYSICAL CONSDITIONS
        FOR j=1, Before_Accretion-1 DO BEGIN
           IF (j eq 1) then begin
           	startiming = 0.0e0
           ENDIF ELSE BEGIN
           	startiming = array[i].time[j]/year
           ENDELSE
           IF keyword_set(allAvs) THEN BEGIN
           PRINTF, 01, FORMAT = '(i8, TR2, 68(E11.4, TR2))', $
             [array[i].ID, array[i].pos[0,j], array[i].pos[1,j], array[i].pos[2,j],$
              array[i].rho[j], array[i].Tgas[j], array[i].Tdust[j], 0.E0, 0.E0,    $
              array[i].AvEff[j], CRP, 0.e0, 0e0, 0e0,                      $
              array[i].H2ShieldFactor[j],array[i].COShieldFactor[j],                 $
              startiming, array[i].time[j+1]/year, 0e0, 0.e0, 0.e0,array[i].Av[*,j]]
           ENDIF ELSE BEGIN
           PRINTF, 01, FORMAT = '(i8, TR2, 68(E11.4, TR2))', $
             [array[i].ID, array[i].pos[0,j], array[i].pos[1,j], array[i].pos[2,j],$
              array[i].rho[j], array[i].Tgas[j], array[i].Tdust[j], 0.E0, 0.E0,    $
              array[i].AvEff[j], CRP, 0.e0, 0e0, 0e0,                      $
              array[i].H2ShieldFactor[j],array[i].COShieldFactor[j],                 $
              startiming, array[i].time[j+1]/year, 0e0, 0.e0, 0.e0]
           ENDELSE
        ENDFOR
     ENDIF
  ENDFOR
  CLOSE, 01
END


;
; This procedure is similar to the above, but this is used to find the equilivbrium
; abundances at each physical time steps. For this purpose we read in the SPH
; particle database, select the particles of interest, and then write the ALCHEMIC
; input file, that contains as many "cells" as time steps in the physical conditions.
; For each time step, the chemical evolution with the fixed condition is run for
; 10^10 years.
;
; The read_steady_state_abuns.pro contains procedures to read the resulting data.
;
; Midofications:
; 07.09.2017  iT keyword added to manually chose the timestep, instead of 
;             relying on the tmod -> array[0].time search

PRO gadget2alchemic_SbS, database, envname=envname, tmod=tmod, $
        dbtype=dbtype, idarray=idarray, tfinal=tfinal, Nt=Nt, $
        allAvs=allAvs, iT=iT
  IF NOT KEYWORD_SET(form) then form = 1
  year = 3.1556952E7
  RESTORE, database

  IF NOT KEYWORD_SET(dbtype) THEN dbtype=1       ; 1 if snapshot, 0 if dump
  IF (dbtype = 1) THEN array = tracedpart
  IF (dbtype = 0) THEN array = hist
  IF (dbtype ne 1) AND (dbtype ne 0) THEN BEGIN
    PRINT, 'Invailed database type!' & STOP
  ENDIF
  IF NOT KEYWORD_SET(envname) THEN envname = '1environ_0d.inp'
  IF NOT KEYWORD_SET(idarray) THEN idarray = [1035326UL]

; Set up the times
;  IF KEYWORD_SET(tfinal) THEN BEGIN
;    t0 = 1.0D-2 *year    ; in seconds
;    tf = tfinal * year  ; in seconds
;    if not KEYWORD_SET(nt) then Nt = 101
;    dt = alog10(tf-t0)/(Nt-1)
;    t = 10.^(dindgen(Nt)*dt) + t0
;  ENDIF ELSE BEGIN
;   t = array[0].time   ; in seconds
;  ENDELSE

; Find the closest timestep to chosen time (tmod)
  IF NOT KEYWORD_SET(tmod) THEN  tmod_s = array[0].time[-1] else tmod_s = tmod * year
  diff = abs(array[0].time - tmod_s)
  if not keyword_set(iT) then begin
    iT = WHERE( diff EQ min(diff))+1
  endif else tmod = array[0].time[iT] / year
  
  PRINT, "Closest snapshot to t=", tmod, "years is #", iT

  CRP = 2.00e-17    ; Cosmic ray ionisation rate of H2
                    ; This is the equivalent for H_CRP = 1E-17
  OPENW, 01, envname
  pn = 0                                                   ; number of particles to write out
; WRITE 1environ_0d.inp HEADER
  PRINTF, 01, '# !Static! ID x y z rho Tg  Td  G0  AvSt  AvIS  ZetaCR  ZetaX fH2_St  fCO_St fH2_IS  fCO_IS T00 T01 Hr  T0  TF'
  PRINTF, 01, FORMAT = '(1(I4))', [N_ELEMENTS(idarray)]
  PRINTF, 01, FORMAT = '(1(I4))', [1]
  FOR i=0, N_ELEMENTS(array.id)-1 DO BEGIN
     IF (WHERE(array[i].id EQ idarray) NE -1) THEN BEGIN
        PRINT, 'Writing data of particle: ', STRCOMPRESS(STRING(array[i].ID))
        Before_Accretion = n_elements(where(array[i].time gt 0. ))+1
        iTc = min([Before_Accretion,iT])
        PRINTF, 01, FORMAT = '(1(I4))', [iTc-1]
; WRITE PHYSICAL CONSDITIONS
        FOR j=0, iTc-2 DO BEGIN
           startiming = array[i].time[j]/year
           IF keyword_set(allAvs) THEN BEGIN
           PRINTF, 01, FORMAT = '(i8, TR2, 68(E11.4, TR2))',                                 $
             [array[i].ID, array[i].pos[0,j], array[i].pos[1,j], array[i].pos[2,j],          $
              array[i].rho[iTc-1], array[i].Tgas[iTc-1], array[i].Tdust[iTc-1], 0.E0, 0.E0,  $
              array[i].AvEff[iTc-1], CRP, 0.e0, 0e0, 0e0,                                    $
              array[i].H2ShieldFactor[iTc-1],array[i].COShieldFactor[iTc-1],                 $
              startiming, array[i].time[j+1]/year, 0e0, 0.e0, 0.e0,array[i].Av[*,iTc-1]]
           ENDIF ELSE BEGIN
           PRINTF, 01, FORMAT = '(i8, TR2, 68(E11.4, TR2))',                                 $
             [array[i].ID, array[i].pos[0,j], array[i].pos[1,j], array[i].pos[2,j],          $
              array[i].rho[iTc-1], array[i].Tgas[iTc-1], array[i].Tdust[iTc-1], 0.E0, 0.E0,  $
              array[i].AvEff[iTc-1], CRP, 0.e0, 0e0, 0e0,                                    $
              array[i].H2ShieldFactor[iTc-1],array[i].COShieldFactor[iTc-1],                 $
              startiming, array[i].time[j+1]/year, 0e0, 0.e0, 0.e0]
           ENDELSE
        ENDFOR
     ENDIF
  ENDFOR
  CLOSE, 01
END
