;+ 
; In this version the reactions are also read in
;     r1 first reactant
;     r2 second reactant
;     products (p1, p2, p3, p4, p5)
; USE old=1 option to read in ALCHEMIC output without
;    reaction rates
;
;
;
;
;+
function read_alchemic81, fname, short=short
   aMp=1.6724E-24
  
  if not KEYWORD_SET(fname) then fname = 'irdc_1.idl'
  if not keyword_set(short) then long=1 else long = 0
  nb_lines = file_lines(fname)
  
  tmp = '' & spec='' & rates=''
  ID = 0UL
  OPENR, 10, fname
  
   READF, 10, FORMAT='(a18)',tmp
   
   READF, 10, FORMAT='(a80)', spec
   READF, 10, FORMAT='(a80)', rates
   READF, 10, FORMAT='(I5)', ipon
   READF, 10, FORMAT='(I5)', Nr
   READF, 10, FORMAT='(I5)', Nz
   READF, 10, Rs
   READF, 10, Zs
   READF, 10, T
   READF, 10, rho
   READF, 10, rhod
   READF, 10, mdmg
   READF, 10, grain
   READF, 10, AvSt
   READF, 10, AvIS
   READF, 10, G0
   READF, 10, ZetaCR
   READF, 10, ZetaX
   READF, 10, gdens
   READF, 10, albedo_UV
   READF, 10, tlast
   READF, 10, tfirst

   READF, 10, FORMAT='(I4)', nfrc   
   y0 = MAKE_ARRAY(nfrc,/string,VALUE='')
   frc = MAKE_ARRAY(nfrc,/DOUBLE,VALUE=0.0D0)
  for j=0, nfrc-1 do begin 
     line = ''
     READF, 10, line
     line = strsplit(line, /extract)
;     print, line
     y0[j] = STRCOMPRESS(line[0], /REMOVE_ALL)
     frc[j] = double(line[1])
  endfor 

   READF, 10, ns
   print, ns
   s = replicate('',ns)
   IF KEYWORD_SET(long) THEN species_format = '((9(A12,2x)),:)' $
   ELSE species_format = '((9(A8,2x)),:)'
   READF, 10, FORMAT= species_format, s
   
   for j=0, N_ELEMENTS(s)-1 do begin
     s[j] = STRCOMPRESS(s[j], /REMOVE_ALL)
   endfor
   
   READF, 10, nstep
   times = MAKE_ARRAY(nstep,/DOUBLE,VALUE=0.0D0)
   READF, 10, FORMAT='(10D12.5)', times
      
   READF, 10, nre

      r1 = replicate('',nre)
      r2 = replicate('',nre)
      p1 = replicate('',nre)
      p2 = replicate('',nre)
      p3 = replicate('',nre)
      p4 = replicate('',nre)
      p5 = replicate('',nre)      
      READF, 10, FORMAT= '((9(A12,2x)),:)', r1
      READF, 10, FORMAT= '((9(A12,2x)),:)', r2
      READF, 10, FORMAT= '((9(A12,2x)),:)', p1
      READF, 10, FORMAT= '((9(A12,2x)),:)', p2
      READF, 10, FORMAT= '((9(A12,2x)),:)', p3
      READF, 10, FORMAT= '((9(A12,2x)),:)', p4
      READF, 10, FORMAT= '((9(A12,2x)),:)', p5
      r1 = strcompress(r1,/remove_all)
      r2 = strcompress(r2,/remove_all)
      p1 = strcompress(p1,/remove_all)
      p2 = strcompress(p2,/remove_all)
      p3 = strcompress(p3,/remove_all)
      p4 = strcompress(p4,/remove_all)
      p5 = strcompress(p5,/remove_all)
      
      ak=MAKE_ARRAY(nre,/DOUBLE,VALUE=0.0D0)
      READF, 10, FORMAT='(10D12.5)',ak

   abundances = MAKE_ARRAY(nstep,ns,/DOUBLE,VALUE=0.0D0)
   READF, 10, FORMAT='(10D12.5)', abundances
 
   close,10
  return, {spec:spec,rates:rates,ipon:ipon,id:id,Rs:Rs,Zs:Zs,Tg:T,rho:rho,rhod:rhod,mdmg:mdmg,grain:grain,$
     AvSt:AvSt,AvIS:AvIs,G0:G0,ZetaCR:ZetaCR,ZetaX:ZetaX,$
     gdens:gdens,albedo_UV:albedo_UV,tlast:tlast,tfirst:tfirst,y0:y0,frc:frc,s:s,times:times,r1:r1,r2:r2,p1:p1,$
     p2:p2,p3:p3,p4:p4,p5:p5,ak:ak,abundances:abundances}

end
