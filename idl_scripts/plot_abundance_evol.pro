@read_alchemic.pro
@compute_H3p.pro
pro plot_abundance_evol, alchemic_file, tracedpart_file,spec=spec,$
    tracednum=tracednum
    
    if not keyword_set(alchemic_file) then alchemic_file = $
        'irdc_00252247_full_grain.idl'
    if not keyword_set(tracedpart_file) then tracedpart_file = $
        '/home/laszlo/Work/alchemic/source/timedependent/testing/tp_003.sav'
    if not keyword_set(spec) then spec = 'CO'
    if not keyword_set(tracednum) then tracednum = 100
    alchemic = read_alchemic(alchemic_file)

    restore,tracedpart_file
    gadget=tracedpart
    @natconst.pro

    fname = 'alchemic_vs_gadget_'+ spec +'.eps'
    !p.thick = 2
    !p.charsize = 0.9
    !p.charthick = 1.5
    axischarsize = 1.2
    axisthick = 4
    SET_PLOT, 'ps'
    !p.font=0
    DEVICE,filename=fname, xsize=11,ysize=8, BITS_PER_PIXEL=8, $
        /ENCAPSUL, /color, /cmyk,/Helvetica
    
    ico_alc = where(alchemic.s eq 'CO')
    ico_traced = where(tracedpart[tracednum].species eq 'CO')
    
    ic_alc = where(alchemic.s eq 'C')
    ic_traced = where(tracedpart[tracednum].species eq 'C')

    icp_alc = where(alchemic.s eq 'C+')
    icp_traced = where(tracedpart[tracednum].species eq 'C+')
    
    io_alc = where(alchemic.s eq 'O')
    io_traced = where(tracedpart[tracednum].species eq 'O')        

    plot,gadget.time/year,gadget.chem[ico_traced ,*],$
        /ylog,xtitle=textoidl('time [Myr]'),$
        ytitle=textoidl('Fractional abundance'),$
        /nodata,yrange=[1e-7,1e-3]
    oplot,gadget[tracednum].time/year,gadget[tracednum].chem[iCO_traced,*],linestyle=0,$
       color=cgcolor('blue')
    oplot,alchemic.times,alchemic.abundances[*,iCO_alc],psym=symcat(16),symsize=0.15,$
       color=cgcolor('black')

    oplot,gadget[tracednum].time/year,gadget[tracednum].chem[iCp_traced,*],linestyle=0,$
       color=cgcolor('red')
    oplot,alchemic.times,alchemic.abundances[*,iCp_alc],psym=symcat(16),symsize=0.15,$
       color=cgcolor('black')

    oplot,gadget[tracednum].time/year,gadget[tracednum].chem[iC_traced,*],linestyle=0,$
       color=cgcolor('green')
    oplot,alchemic.times,alchemic.abundances[*,iC_alc],psym=symcat(16),symsize=0.15,$
       color=cgcolor('black')

    oplot,gadget[tracednum].time/year,gadget[tracednum].chem[iO_traced,*],linestyle=0,$
       color=cgcolor('orange')
    oplot,alchemic.times,alchemic.abundances[*,iO_alc],psym=symcat(16),symsize=0.15,$
       color=cgcolor('black')
    
    al_legend,[textoidl('O'),textoidl('C^+'),textoidl('C'),textoidl('CO')], $
      linestyle=0,color=[cgcolor('orange'),cgcolor('red'),cgcolor('green'),cgcolor('blue')],$
        position=[2e6,1e-4],box=0,charsize=0.8,number=9,pspacing=0.23
    al_legend,['gadget'],linestyle=0,color=cgcolor('black'),position=[1.83e6,1e-5],box=0,charsize=0.8,number=9,pspacing=0.23
    al_legend,['alchemic'],psym=16,color=cgcolor('black'),position=[1.9e6,5e-6],box=0,charsize=0.8

    DEVICE, /close
    SET_PLOT, 'x'
  
      SET_PLOT, 'ps'
    !p.font=0  
    DEVICE,filename='alchemic_vs_gadget_electr.eps', xsize=11,ysize=8, BITS_PER_PIXEL=8, $
        /ENCAPSUL, /color, /cmyk,/Helvetica
    
    ie_alc = where(alchemic.s eq 'ELECTR')
    ie_traced = where(tracedpart[tracednum].species eq 'ELECTR')
    
    ihcop_alc = where(alchemic.s eq 'HCO+')
    ihcop_traced = where(tracedpart[tracednum].species eq 'HCO+')

    ih3p_alc = where(alchemic.s eq 'H3+')
    ih3p_traced = compute_H3p(tracedpart,id=[215090l])
    
    ihp_alc = where(alchemic.s eq 'H+')
    ihp_traced = where(tracedpart[tracednum].species eq 'H+')        

    plot,gadget.time/year,gadget.chem[ie_alc,*],$
        /ylog,xtitle=textoidl('time [Myr]'),$
        ytitle=textoidl('Fractional abundance'),$
        /nodata,yrange=[1e-14,1e-4]
    oplot,gadget[tracednum].time/year,gadget[tracednum].chem[ie_traced,*],linestyle=0,$
       color=cgcolor('blue')
    oplot,alchemic.times,alchemic.abundances[*,ie_alc],psym=symcat(16),symsize=0.15,$
       color=cgcolor('black')

    oplot,gadget[tracednum].time/year,gadget[tracednum].chem[ihcop_traced,*],linestyle=0,$
       color=cgcolor('red')
    oplot,alchemic.times,alchemic.abundances[*,ihcop_alc],psym=symcat(16),symsize=0.15,$
       color=cgcolor('black')

    oplot,gadget[tracednum].time/year,ih3p_traced,linestyle=0,$
       color=cgcolor('green')
    oplot,alchemic.times,alchemic.abundances[*,ih3p_alc],psym=symcat(16),symsize=0.15,$
       color=cgcolor('black')

    oplot,gadget[tracednum].time/year,gadget[tracednum].chem[ihp_traced,*],linestyle=0,$
       color=cgcolor('orange')
    oplot,alchemic.times,alchemic.abundances[*,ihp_alc],psym=symcat(16),symsize=0.15,$
       color=cgcolor('black')
    
    al_legend,[textoidl('H^+'),textoidl('e^-'),textoidl('H_3^+'),textoidl('HCO^+')], $
      linestyle=0,color=[cgcolor('orange'),cgcolor('blue'),cgcolor('green'),cgcolor('red')],$
        position=[1.9e6,6e-5],box=0,charsize=0.8,number=9,pspacing=0.23
    al_legend,['gadget'],linestyle=0,color=cgcolor('black'),position=[1.83e6,5e-8],box=0,charsize=0.8,number=9,pspacing=0.23
    al_legend,['alchemic'],psym=16,color=cgcolor('black'),position=[1.9e6,1e-8],box=0,charsize=0.8

    DEVICE, /close
    SET_PLOT, 'x' 
    
end
