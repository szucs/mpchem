Function read_data_olli, file, ncoll=ncoll, nt = nt, n0 = n0
  @natconst.pro
  if not keyword_set(file) then file = 'result_new_noquantum.dat'
  if not keyword_set(ncoll) then ncoll = 1398  ; +1 time 
  if not keyword_set(nt) then nt = 100    ; +1 header
  if not keyword_Set(n0) then n0 = 1e5    ; particles per cubic cm

  spec = replicate('',ncoll)
  abuns = dblarr(nt,ncoll)
  times = dblarr(nt)

  openr,1,file
    TextTmp = ''
    NumTmp = dblarr(ncoll+1)
    readf,1,TextTmp
    spec = (strsplit(TextTmp,/extract))[1:*]

    for i=0, nt-1 do begin
        readf,1,NumTmp
        times[i] = NumTmp[0]
        abuns[i,*] = NumTmp[1:*]
    endfor
  close,1
  
  for i=0, ncoll-1 do begin
     
     grsurf =  STRPOS(spec[i],'*')
     if grsurf ne -1 then begin
        spec[i] = 'g' + STRMID(spec[i], 0, grsurf)
     endif
  
  endfor
  
  return, {spec: spec, times:times/year, abuns:abuns/n0}
  
end
