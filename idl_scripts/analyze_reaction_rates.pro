function destruction_rates, data, tt, spec=spec, rstring=rstring, amu=amu, $
           rates=rates
@natconst
if not keyword_Set(amu) then amu = 1.43
if not keyword_set(spec) then begin
   spec = 'CO'
   print, 'WARNING_DESTRUCTION_RATES: spec was not set, using CO!'
endif
; XX reactants

nr1_co = where(data.r1 eq spec)
nr2_co = where(data.r2 eq spec)

nr_co = [nr1_co, nr2_co]
nr_co = nr_co[uniq(nr_co, sort(nr_co))]
nr_co = nr_co(sort(nr_co))
nr_co = nr_co[where(nr_co ge 0)]

;print, data.r1[nr_co], data.r2[nr_co]
if n_elements(data.RHO) gt 1 then begin
  nH = data.RHO[tt] / (amu * mp)
  rr = data.ak[tt,nr_co] 
endif else begin
  nH = data.RHO / (amu * mp)
  rr = data.ak[nr_co] 
endelse

if keyword_set(rates) then begin
   return, rr
   stop
endif

if keyword_set(rstring) then begin
; then the function return the eq. of ALL reactions destructing CO, in 
; the order given by nr_co
   str = replicate('',n_elements(nr_co))   
   for i=0, n_elements(nr_co)-1 do begin
      str[i] = data.r1[nr_co[i]]+' + '+data.r2[nr_co[i]]+' -> '+data.p1[nr_co[i]]
      if data.p2[nr_co[i]] ne '' then str[i] = str[i]+' + '+data.p2[nr_co[i]]
      if data.p3[nr_co[i]] ne '' then str[i] = str[i]+' + '+data.p3[nr_co[i]]
      if data.p4[nr_co[i]] ne '' then str[i] = str[i]+' + '+data.p4[nr_co[i]]
      if data.p5[nr_co[i]] ne '' then str[i] = str[i]+' + '+data.p5[nr_co[i]]
   endfor
   return, str 
   stop
endif

for i=0, n_elements(rr)-1 do begin
   rr[i] = rr[i] * data.abundances[tt,where(data.s eq data.r1[nr_co[i]])] * nH
   if data.r2[nr_co[i]] ne 'CRPHOT' and  data.r2[nr_co[i]] ne 'PHOTON' and data.r2[nr_co[i]] ne 'XRAYS' $
     and data.r2[nr_co[i]] ne 'CRP' and data.r2[nr_co[i]] ne 'DESORB' and data.r2[nr_co[i]] ne 'FREEZE' $
     then rr[i] =rr[i] * data.abundances[tt,where(data.s eq data.r2[nr_co[i]])] * nH
endfor

return, rr
end

function production_rates, data, tt, spec=spec, rstring=rstring, amu=amu, $
           rates=rates
@natconst
if not keyword_Set(amu) then amu = 1.43
;  CO produced
np1_co = where(data.p1 eq spec)
np2_co = where(data.p2 eq spec)
np3_co = where(data.p3 eq spec)
np4_co = where(data.p4 eq spec)    ;no reactions producing CO as a p4 or p5
np5_co = where(data.p5 eq spec)

np_co = [np1_co, np2_co, np3_co, np4_co, np5_co]
np_co = np_co[uniq(np_co, sort(np_co))]
np_co = np_co(sort(np_co))
np_co = np_co[where(np_co ge 0)]

if keyword_set(rstring) then begin
   str = replicate('',n_elements(np_co))   
   for i=0, n_elements(np_co)-1 do begin

   str[i] = data.r1[np_co[i]]+' + '+data.r2[np_co[i]]+' -> '+data.p1[np_co[i]]
   if data.p2[np_co[i]] ne '' then str[i] = str[i]+' + '+data.p2[np_co[i]]
   if data.p3[np_co[i]] ne '' then str[i] = str[i]+' + '+data.p3[np_co[i]]
   if data.p4[np_co[i]] ne '' then str[i] = str[i]+' + '+data.p4[np_co[i]]
   if data.p5[np_co[i]] ne '' then str[i] = str[i]+' + '+data.p5[np_co[i]]
   endfor
   return, str 
   stop
endif


; sort reaction by rate


if n_elements(data.RHO) gt 1 then begin
   nH = data.RHO[tt] / (amu * mp)
   rr = data.ak[tt,np_co] 
endif else begin
   nH = data.RHO / (amu * mp)
   rr = data.ak[np_co]
endelse

if keyword_set(rates) then begin
   return, rr
   stop
endif

for i=0, n_elements(rr)-1 do begin
   rr[i] = rr[i] * data.abundances[tt,where(data.s eq data.r1[np_co[i]])] * nH
   if data.r2[np_co[i]] ne 'CRPHOT' and  data.r2[np_co[i]] ne 'PHOTON' and data.r2[np_co[i]] ne 'XRAYS' $
     and data.r2[np_co[i]] ne 'CRP' and data.r2[np_co[i]] ne 'DESORB' and data.r2[np_co[i]] ne 'FREEZE' $
     then rr[i] =rr[i] * data.abundances[tt,where(data.s eq data.r2[np_co[i]])] * nH
endfor

return, rr
end

@read_alchemic
@read_alchemic81.pro
pro ANALYZE_REACTION_RATES, filename, ps=ps, spec=spec, sig_dest=sig_dest,$
    sig_prod=sig_prod,short=short,xlog=xlog,ylog=ylog,outname=outname,pdf=pdf,$
    xrange=xrange,alchemic81=alchemic81, amu=amu, rates=rates
; reading data
;                                                                                           !!!READ 'RAW' ALCHEMIC FILE INSTEAD .sav
if not keyword_set(spec) then spec = 'CO'
if not keyword_set(filename) then filename = 'irdc_01005515.idl'
if not keyword_set(alchemic81) then a = read_alchemic(filename,short=short) $
else a = read_alchemic81(filename,short=short)
if not keyword_set(sig_dest) then sig_dest = 1e-2
if not keyword_set(sig_prod) then sig_prod = 1e-2
csfactor = 2
if keyword_Set(pdf) then csfactor = 1
!p.charsize = 1.5 * csfactor
;
; Destruction
;
nt = n_elements(a.times)

nr1_co = where(a.r1 eq spec)
nr2_co = where(a.r2 eq spec)

nr_co = [nr1_co, nr2_co]
nr_co = nr_co[uniq(nr_co, sort(nr_co))]
nr_co = nr_co[where(nr_co ge 0)]
nrco = n_elements(nr_co)

rr = {time:a.times, rrco: replicate(!values.d_nan, nt, nrco), rdrates: replicate(!values.d_nan, nt, nrco)}
react=[-55]
for i=0, nt-1 do begin
    tmp = destruction_rates(a,i,spec=spec,amu=amu)
    TMPRates = destruction_rates(a,i,spec=spec,amu=amu,/rates)
        
    rr.rrco[i,*] = tmp
    rr.rdrates[i,*] = TMPRates
    react0 = where(tmp gt total(tmp)*sig_dest)
    react = append(react, react0)
endfor
react = react[where(react ge 0)]
react = react[sort(react)]
react = react[uniq(react)]

rrstring = destruction_rates(a,0,/rstring,spec=spec,amu=amu)

plottarr = replicate(!values.D_nan, nt, n_elements(react))
PlotRates = replicate(!values.D_nan, nt, n_elements(react))
plotstr  = rrstring[react]
for i=0, nt-1 do begin
   plottarr[i,*] = rr.rrco[i, react]
   PlotRates[i,*] = rr.rdrates[i, react]
endfor

if KEYWORD_SET(ps) or KEYWORD_SET(pdf) then begin
  SET_PLOT, 'ps'
  if not keyword_set(outname) then outname = spec+'_prod-destr.eps'
  DEVICE,filename=outname,  xsize=30,ysize=8, BITS_PER_PIXEL=8, /ENCAPSUL, /color
endif else window,1,title='CO destruction!', xsize=1400*1.5, ysize=500*1.5

colarr = indgen(n_elements(react)) * 0
ltarr  = indgen(n_elements(react)) * 0

LoadCT, 4
dc = 255 / n_elements(react)
color = 0

!p.multi = [0,3,1,0,1]

; plot the total CO distruction rate
; sum the rates
destratetot = replicate(!values.d_nan, nt)
for i=0, nt-1 do begin
   destratetot[i] = total(rr.rrco[i,*])
endfor

;plot, a.times, destratetot, /ylog,xtitle='Time [yr]',$
;      ytitle='Total destruction rate', title='Destruction of '+spec,$
;      xlog=xlog

if keyword_Set(rates) then plottarr = PlotRates

plot, a.times, plottarr[*,0] , /nodata, /ylog,yrange=[max([min(plottarr),1e-20]),min([max(plottarr),1.])],$
      xtitle='Time [yr]', ytitle='Reaction rate', title='Destruction of '+spec,$
      xlog=xlog, xrange=xrange
if not keyword_Set(rates) then  oplot, a.times, destratetot, linestyle=0, thick=4
ll = 1
for i=0, n_elements(react)-1 do begin
   if ll ge 6 then ll = 0
   oplot, a.times, plottarr[*,i], color=color, $
          linestyle=ll, thick=2
   colarr[i] = color
   ltarr[i] = ll
   color=color+dc
   ll = ll + 1
endfor

al_legend, ['Total destruction rate',plotstr], color=[0,colarr], linestyle=[0,ltarr], charsize=0.6*csfactor,/right,/bottom,$
     thick=[4,replicate(2,n_elements(colarr)) ],background_color=!p.background


;
; production

np1_co = where(a.p1 eq spec)
np2_co = where(a.p2 eq spec)
np3_co = where(a.p3 eq spec)
np4_co = where(a.p4 eq spec)    ;no reactions producing CO as a p4 or p5
np5_co = where(a.p5 eq spec)

np_co = [np1_co, np2_co, np3_co, np4_co, np5_co]
np_co = np_co[where(np_co ge 0)]
np_co = np_co[uniq(np_co, sort(np_co))]
npco = n_elements(np_co)

rp = {time:a.times, rpco: replicate(!values.d_nan, nt, npco), rprates: replicate(!values.d_nan, nt, npco)}
reactp=[-55]
for i=0, nt-1 do begin
    tmp = production_rates(a,i,spec=spec,amu=amu)
    TMPRates = production_rates(a,i,spec=spec,amu=amu,/rates)
    
    rp.rpco[i,*] = tmp
    rp.rprates[i,*] = TMPRates
    reactp0 = where(tmp gt total(abs(tmp))*sig_prod)
    reactp = append(reactp,reactp0)
endfor
reactp = reactp[where(reactp ge 0.)]
reactp = reactp[sort(reactp)]
reactp = reactp[uniq(reactp)]
rpstring = production_rates(a,0,/rstring,spec=spec,amu=amu)

plottarrp = replicate(!values.D_nan, nt, n_elements(reactp))
PlotRates = replicate(!values.D_nan, nt, n_elements(reactp))
plotstrp  = rpstring[reactp]
for i=0, nt-1 do begin
   plottarrp[i,*] = rp.rpco[i, reactp]
   PlotRates[i,*] = rp.rprates[i, reactp]
endfor


LoadCT, 4
dc = 255 / n_elements(reactp)
color = 0

colarr = indgen(n_elements(reactp)) * 0
ltarr  = indgen(n_elements(reactp)) * 0

; plot the total CO distruction rate
; sum the rates
prodratetot = replicate(!values.d_nan, nt)
for i=0, nt-1 do begin
   prodratetot[i] = total(rp.rpco[i,*])
endfor

;plot, a.times, prodratetot, /ylog,xtitle='Time [yr]', $
;      ytitle='Total production rate', title='Production of '+spec,$
;      xlog=xlog

if keyword_Set(rates) then plottarrp = PlotRates

plot, a.times, plottarrp[*,0], /nodata, /ylog, yrange=[max([min(plottarrp),1e-20]),min([max(plottarrp),1.])],$
      xtitle='Time [yr]', ytitle='Reaction rate', title='Production of '+spec,$
      xlog=xlog, xrange=xrange

if not keyword_Set(rates) then oplot, a.times, prodratetot, linestyle=0, thick=4
ll = 1
for i=0, n_elements(reactp)-1 do begin
   if ll ge 6 then ll = 0
   oplot, a.times, plottarrp[*,i], color=color, $
          linestyle=ll, thick=2
   colarr[i] = color
   ltarr[i] = ll
   color=color+dc
   ll = ll + 1
endfor


al_legend, ['Total production rate',plotstrp], color=[0,colarr], linestyle=[0,ltarr], charsize=0.6*csfactor,/right,/bottom,$
     thick=[4,replicate(2,n_elements(colarr)) ],background_color=!p.background


plot, a.times, prodratetot, /nodata, /ylog, yrange=[max([min([prodratetot,destratetot]),1e-20]),min([max([prodratetot,destratetot]),1.])],$
      xtitle='Time [Myr]', ytitle='Reaction rate', title='Total rates',$
      xlog=xlog, xrange=xrange

oplot, a.times, destratetot, linestyle=0, thick=2,color=cgcolor('red')
oplot, a.times, prodratetot, linestyle=0, thick=2,color=cgcolor('green')

al_legend, ['Total destruction rate','Total production rate'], color=[cgcolor('red'),cgcolor('green')], linestyle=0, charsize=0.6*csfactor,/right,/bottom,background_color=!p.background

if keyword_set(ps) or KEYWORD_SET(pdf) then begin
device, /close
set_plot, 'x'
endif

if keyword_set(pdf) then begin
   spawn,'ps2pdf14 -dProcessColorModel=/DeviceCMYK -dEPSCrop ' + outname
   spawn,'rm ' + outname
endif

!p.multi=0
loadct,1

end
