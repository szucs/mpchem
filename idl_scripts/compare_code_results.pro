@read_alchemic.pro
@read_alchemic_old.pro

pro compare_code_results,old,new,long=long
  set_plot, 'ps'
  device,filename='comparison.eps', xsize=25,ysize=25, BITS_PER_PIXEL=8, $
         /ENCAPSUL, /color, /cmyk
  IF NOT KEYWORD_SET(old) THEN old = "unmodified_1.idl"
  IF NOT KEYWORD_SET(new) THEN new = "irdc_00000001.idl"
  unmodified = read_alchemic_old(old,long=long)
  time_dependent = read_alchemic(new,long=long)

  index_CO = where(unmodified.s eq 'CO')
  index_HCOp = where(unmodified.s eq 'HCO+')
  index_CH3OH = where(unmodified.s eq 'CH3OH')
  index_H2O = where(unmodified.s eq 'H2O')
  
  !p.multi = [0, 2, 2, 0, 0]
  
  plot, unmodified.times, unmodified.abundances[*,index_CO], thick=3,$
     title = 'CO fractional abundance',xtitle='t [yr]',ytitle='fractional abundance'
  oplot, time_dependent.times, time_dependent.abundances[*,index_CO],$
     psym=1, thick=3
  
  al_legend, ['Unmodified','Time-dependent'], thick=3, linestyle=[0,2]
  
  plot, unmodified.times, unmodified.abundances[*,index_HCOp], thick=3,$
     title = 'HCO+ fractional abundance',xtitle='t [yr]',ytitle='fractional abundance'
  oplot, time_dependent.times, time_dependent.abundances[*,index_HCOp],$
     psym=1, thick=3
     
   al_legend, ['Unmodified','Time-dependent'], thick=3, linestyle=[0,2]
     
  plot, unmodified.times, unmodified.abundances[*,index_CH3OH], thick=3,$
     title = 'CH3OH fractional abundance',xtitle='t [yr]',ytitle='fractional abundance'
  oplot, time_dependent.times, time_dependent.abundances[*,index_CH3OH],$
     psym=1, thick=3
   
   al_legend, ['Unmodified','Time-dependent'], thick=3, linestyle=[0,2]
     
  plot, unmodified.times, unmodified.abundances[*,index_H2O], thick=3,$
     title = 'H2O fractional abundance',xtitle='t [yr]',ytitle='fractional abundance'
  oplot, time_dependent.times, time_dependent.abundances[*,index_H2O],$
     psym=1, thick=3
       
   al_legend, ['Unmodified','Time-dependent'], thick=3, linestyle=[0,2]
       
  device, /close
  set_plot, 'x'
end
