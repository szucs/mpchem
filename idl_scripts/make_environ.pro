PRO make_environ, envname, testcase=testcase, treecol=treecol
 if not keyword_set(testcase) then testcase = 1
 if not keyword_set(envname) then envname = '1environ.inp'

 mp  = 1.6726d-24

 ZetaCR = 1d-17  ;Dima's version 1.3000e-17
 ZetaX  = 0.0d0
 G0     = 0.0d0
 Av_St  = 0.0d0
 fH2_St = 0.0d0
 fCO_St = 0.0d0

; test case A: constant physical parameters
IF testcase eq 1 THEN BEGIN
 t0 = 0.D0          ; in years
 tf = 3.0D6         ; in years
 Nt = 101
 dt = (tf-t0)/(Nt-1)
 t = dindgen(Nt)*dt + t0

 ID = 1
 x = 0d0
 y = 0d0
 z = 0d0

 G0 = 0d0
 nH = 1e5
 mu = 1.43              ; cm-3
 rho = nH * mu * mp       ; in g cm-3
 Tg = 20.                     ; in K
 Td = 20.                     ; in K
 AvSt = 0
 AvIS = 8.                   ; Av
 if keyword_set(treecol) then AvIS = dblarr(48) + AvIS
 fH2_St = 1e-9
 fCO_St = 1e-9
 fH2_IS = 4.68E-8             ; calculated for NH2 = 1E21
 fCOss_IS = 0.0138            ; calculated for NH2 = 1E21, xCO = 1e-4
 fCOh2s_IS = 0.484
 ENDIF ELSE IF testcase eq 2 THEN BEGIN
; Sipila et al. 2015 model testcase
  t0 = 0.D0          ; in years
  tf = 1.0D7         ; in years
  Nt = 101
  dt = (tf-t0)/(Nt-1)
  t = dindgen(Nt)*dt + t0

  ZetaCR = 1.3d-17

  ID = 1
  x = 0d0
  y = 0d0
  z = 0d0

  G0 = 0d0
  nH = 1e5
  mu = 1.43              ; cm-3
  rho = nH * mu * mp       ; in g cm-3
  Tg = 10.                     ; in K
  Td = 10.                     ; in K
  AvSt = 0
  AvIS = 10.                   ; Av
  if keyword_set(treecol) then AvIS = dblarr(48) + AvIS
  fH2_St = 1e-9
  fCO_St = 1e-9
  fH2_IS = 4.68E-8             ; calculated for NH2 = 1E21
  fCOss_IS = 0.0138            ; calculated for NH2 = 1E21, xCO = 1e-4
  fCOh2s_IS = 0.484
ENDIF ELSE IF testcase EQ 3 THEN BEGIN
; test case B: constant physical parameters (from Tobias)
 t0 = 0.D0          ; in years
 tf = 1.0D6         ; in years
 Nt = 90
 dt = (tf-t0)/(Nt-1)
 t = dindgen(Nt)*dt + t0

 ID = 1
 x = 0d0
 y = 0d0
 z = 0d0

 G0 = 0d0
 nH = 1e5                   ; cm-3
 mu = 1.43
 rho= nH * mu * mp                ; in g cm-3
 Tg = 10.                     ; in K
 Td = 10.                     ; in K
 AvSt = 1000.
 AvIS = 10.                   ; Av
 ZetaCR = 1.3d-17
 fH2_St = 1e-9
 fCO_St = 1e-9
 fH2_IS = 1.                  ; calculated for NH2 = 1E21
 fCOss_IS = 1.                ; calculated for NH2 = 1E21, xCO = 1e-4
 fCOh2s_IS = 1.
ENDIF

 fH2_St = fH2_St * replicate(1., n_elements(AvSt))  ;* exp(-2.5d0*AvSt)
 fCO_St = fCO_St * replicate(1., n_elements(AvSt))  ;* exp(-1.7d0*AvSt)
 fH2_IS = fH2_IS * replicate(1., n_elements(AvIS))  ;* exp(-2.5d0*AvIS)
 fCO_IS = fCOss_IS * replicate(1., n_elements(AvIS))  ;* exp(-1.7d0*AvIS)

 OPENW, 01, envname

 PRINTF,01,'# ID x y z rho Tg  Td  G0  AvSt  AvIS  ZetaCR  ZetaX'$
            +' fH2_St  fCO_St fH2_IS  fCO_IS T00 T01 Hr  T0  TF'
 PRINTF,01,1
 PRINTF,01,1
 PRINTF,01,Nt-1

 for i=1, Nt-1 do begin
   PRINTF,01,FORMAT = '(i8, TR2, 20(E11.4, TR2))',[ID,x,y,z,rho,Tg,Td,G0,AvSt,AvIS,$
         ZetaCR,ZetaX,fH2_St,fCO_St,fH2_IS,fCO_IS,t[i-1],t[i],0d0,0d0,0d0]
 endfor
 close,/all
end
;+
; PROGRAM Make_Environ_Tobias
;
; 04.2016
;
; This program sets up a grid of models to be compared to the chemical models
; of Albertsson et al. 2013 - chemical models, with or without o/p and
; deuterium chemistry.
;
; Parameters:
;    density: 300, 3 000 and 30 000 particle per cm^-3
;    Av: 1, 5, 20 mag
;    Tg = Td: 10 K or 25 K
;
; The (self-) shielding factors are calculated asuming a fixed Av to N(H)
; conversion factor and fixed abundances.
;+
@co-shield.pro
PRO make_environ_tobias, envname, testcase=testcase
 if not keyword_set(testcase) then testcase = 1
 if not keyword_set(envname) then envname = '1environ.inp'

 mp  = 1.6726d-24

 ZetaCR = 1d-17  ;Dima's version 1.3000e-17
 ZetaX  = 0.0d0
 G0     = 0.0d0
 Av_St  = 0.0d0
 fH2_St = 0.0d0
 fCO_St = 0.0d0

; test case B: constant physical parameters (from Tobias)
 t0 = 1.D-2          ; in years
 tf = [1E10,1E10,1E10,1E10,1E10,1E10,1E10,1E10,1E10]         ; in years
 Nt = 301
 dt1 = alog10(tf[0]-t0)/(Nt-1)
 t1 = 10.^(dindgen(Nt)*dt1) + t0
 dt2 = alog10(tf[1]-t0)/(Nt-1)
 t2 = 10.^(dindgen(Nt)*dt2) + t0
 dt3 = alog10(tf[2]-t0)/(Nt-1)
 t3 = 10.^(dindgen(Nt)*dt3) + t0

 t = [[t1],[t2],[t3],[t1],[t2],[t3],[t1],[t2],[t3]]
help,t
 ID = [1,5,20,10,50,200,100,500,2000]
 x = 0d0
 y = 0d0
 z = 0d0

 G0 = 1d0
 nH = [300.,300.,300.,3000.,3000.,3000.,30000.,30000.,30000.]                   ; cm-3
 mu = 2.3197418
 rho= nH * mu * mp                ; in g cm-3
 Tg = [25.,25.,25.,25.,25.,25.,25.,25.,25.] ;[10.,10.,10.,10.,10.,10.,10.,10.,10.] ; [20.,20.,20.]                     ; in K
 Td = Tg;[10.,10.,10.,10.,10.,10.,10.,10.,10.]                     ; in K
; Shielding
 AvSt = 1000.
 AvIS = [1.,5.,20.,1.,5.,20.,1.,5.,20.]                   ; Av
 NtotIS = AvIS * 1.8699E21  ; cm^-2
 NcoIS = NtotIS * 1E-4
 Nh2IS = NtotIS * 0.5
 fH2_St = 1.
 fCO_St = 1.
 fH2_IS = calculate_H2_self_shielding(Nh2IS)                  ; calculated for NH2 = 1E21
 fCOss_IS = calculate_CO_self_shielding_factor(NcoIS) * calculate_CO_shielding_by_H2(Nh2IS)
                ; calculated for NH2 = 1E21, xCO = 1e-4

 fH2_St = fH2_St * replicate(1., n_elements(AvSt))  ;* exp(-2.5d0*AvSt)
 fCO_St = fCO_St * replicate(1., n_elements(AvSt))  ;* exp(-1.7d0*AvSt)
 fH2_IS = fH2_IS * replicate(1., n_elements(AvIS))  ;* exp(-2.5d0*AvIS)
 fCO_IS = fCOss_IS * replicate(1., n_elements(AvIS))  ;* exp(-1.7d0*AvIS)

 OPENW, 01, envname

 PRINTF,01,'# ID x y z rho Tg  Td  G0  AvSt  AvIS  ZetaCR  ZetaX'$
            +' fH2_St  fCO_St fH2_IS  fCO_IS T00 T01 Hr  T0  TF'
 PRINTF,01,1
 PRINTF,01,n_elements(rho)

 for jj=0, n_elements(rho)-1 do begin

 PRINTF,01,Nt-1

 for i=1, Nt-1 do begin
   PRINTF,01,FORMAT = '(i8, TR2, 20(E11.4, TR2))',[ID[jj],x,y,z,rho[jj],Tg[jj],Td[jj],G0,AvSt,AvIS[jj],$
         ZetaCR,ZetaX,fH2_St,fCO_St,fH2_IS[jj],fCO_IS[jj],t[i-1,jj],t[i,jj],0d0,0d0,0d0]
 endfor

 endfor

 close,/all

end

;+
; PROGRAM Make_Environ_CRgrid
;
; 03.2016
;
; This code is set up to calculate models to be compared to Tobias Albertsson's
; CRP rate tests.
;+
PRO make_environ_CRgrid, envname

 if not keyword_set(envname) then envname = '1environ.inp'

 mp  = 1.6726d-24

 ZetaCR = [2.7826e-18,3.5938e-18,4.6415e-18,5.9948e-18,7.7426e-18,1.e-17,1.2915e-17,$
           1.6681e-17,2.1544e-17,2.7826e-17,3.5938e-17,4.6415E-17,5.9948E-17, $
           7.7426E-17,1.0000E-16,1.2915E-16,1.6681E-16,2.1544E-16,2.7826E-16, $
           3.5938E-16,4.6415E-16,5.9948E-16,7.7426E-16,1.0000E-15]  ;Dima's version 1.3000e-17
 ZetaX  = 0.0d0
 G0     = 0.0d0
 Av_St  = 1.0d0
 fH2_St = 0.0d0
 fCO_St = 0.0d0

; test case B: constant physical parameters (from Tobias)
 t0 = 0.0E0          ; in years
 tf = 1.0E07         ; in years
 Nt = 100
 dt = alog10(tf-t0)/(Nt-1)
 t = 10.^(dindgen(Nt)*dt) + t0

 ID = indgen(n_elements(ZetaCR))
 x = 0d0
 y = 0d0
 z = 0d0

 G0 = 1d0
 nH = replicate(10000.,n_elements(ZetaCR))                   ; cm-3
 mu = 2.33
 rho= nH * mu * mp                ; in g cm-3
 Tg = [1.0000E+01,1.0000E+01,1.0000E+01,1.0000E+01,1.0000E+01,1.1000E+01,1.2000E+01,$
       1.2400E+01,1.3000E+01,1.3500E+01,1.4520E+01,1.5000E+01,1.6000E+01,1.8200E+01,$
       1.9500E+01,2.0200E+01,2.0500E+01,2.3400E+01,2.6220E+01,3.0000E+01,3.2230E+01,$
       3.4940E+01,3.7300E+01,4.0200E+01]
 Td = Tg                    ; in K
 AvSt = 1000.
 AvIS = replicate(10.,n_elements(Zetacr))                   ; Av
; ZetaCR = 1d-17
 fH2_St = 1.
 fCO_St = 1.
 fH2_IS = replicate(1.,n_elements(Zetacr))                  ; calculated for NH2 = 1E21
 fCOss_IS = replicate(1.,n_elements(Zetacr))                ; calculated for NH2 = 1E21, xCO = 1e-4

 fH2_St = fH2_St * replicate(1., n_elements(AvSt))  ;* exp(-2.5d0*AvSt)
 fCO_St = fCO_St * replicate(1., n_elements(AvSt))  ;* exp(-1.7d0*AvSt)
 fH2_IS = fH2_IS * replicate(1., n_elements(AvIS))  ;* exp(-2.5d0*AvIS)
 fCO_IS = fCOss_IS * replicate(1., n_elements(AvIS))  ;* exp(-1.7d0*AvIS)

 OPENW, 01, envname

 PRINTF,01,'# ID x y z rho Tg  Td  G0  AvSt  AvIS  ZetaCR  ZetaX'$
            +' fH2_St  fCO_St fH2_IS  fCO_IS T00 T01 Hr  T0  TF'
 PRINTF,01,1
 PRINTF,01,n_elements(rho)

 for jj=0, n_elements(ZetaCR)-1 do begin

 PRINTF,01,Nt-1

 for i=1, Nt-1 do begin
   PRINTF,01,FORMAT = '(i8, TR2, 20(E11.4, TR2))',[ID[jj],x,y,z,rho[jj],Tg[jj],Td[jj],G0,AvSt,AvIS[jj],$
         ZetaCR[jj],ZetaX,fH2_St,fCO_St,fH2_IS[jj],fCO_IS[jj],t[i-1],t[i],0d0,0d0,0d0]
 endfor

 endfor

 close,/all

end

;+
; PROGRAM Make_Environ_Sipila
;
; 05.2016
;
; This program sets up a grid of models to be compared to the chemical models
; of Sipila et al. 2015 for protostellar cores
;
; Parameters:
;    density: 300, 3 000 and 30 000 particle per cm^-3
;    Av: 10 mag
;    Tg = Td: 10 K
;    ZetaCR: 1.3D-17 s^-1
;    rho: 1E3, 1E4, 1E5, 1E6
; The (self-) shielding factors are calculated asuming a fixed Av to N(H)
; conversion factor and fixed abundances.
;+
@co-shield.pro
PRO Make_Environ_Sipila, envname
 if not keyword_set(envname) then envname = '1environ.inp'

 mp  = 1.6726d-24

 ZetaCR = 1.3d-17  ;Dima's version 1.3000e-17
 ZetaX  = 0.0d0
 G0     = 0.0d0
 Av_St  = 0.0d0
 fH2_St = 0.0d0
 fCO_St = 0.0d0

; test case B: constant physical parameters (from Tobias)
 t0 = 1.D-2          ; in years
 tf = [1E7,1E7,1E7,1E7]         ; in years
 Nt = 101
 dt1 = alog10(tf[0]-t0)/(Nt-1)
 t1 = 10.^(dindgen(Nt)*dt1) + t0
 dt2 = alog10(tf[1]-t0)/(Nt-1)
 t2 = 10.^(dindgen(Nt)*dt2) + t0
 dt3 = alog10(tf[2]-t0)/(Nt-1)
 t3 = 10.^(dindgen(Nt)*dt3) + t0

 t = [[t1],[t2],[t3],[t1],[t1],[t2],[t3],[t1]]

 ID = [1000,10000,100000,1000000,1001,10001,100001,1000001]
 x = 0d0
 y = 0d0
 z = 0d0

 G0 = 0d0
 nH = [1000.,1E4,1E5,1E6,1000.,1E4,1E5,1E6]                   ; cm-3
 mu = 1.4  ;2.3197418
 rho= nH * mu * mp                ; in g cm-3
 Tg = [10.,10.,10.,10.,30.,30.,30.,30.]                     ; in K
 Td = Tg;[10.,10.,10.,10.,10.,10.,10.,10.,10.]                     ; in K
; Shielding
 AvSt = 0.
 AvIS = [10.,10.,10.,10.,1.,1.,1.,1.]                   ; Av
 NtotIS = AvIS * 1.8699E21  ; cm^-2
 NcoIS = NtotIS * 1E-4
 Nh2IS = NtotIS * 0.5
 fH2_St = 1.
 fCO_St = 1.
 fH2_IS = 1. ;calculate_H2_self_shielding(Nh2IS)                  ; calculated for NH2 = 1E21
 fCOss_IS = 1. ;calculate_CO_self_shielding_factor(NcoIS) * calculate_CO_shielding_by_H2(Nh2IS)
                ; calculated for NH2 = 1E21, xCO = 1e-4

 fH2_St = fH2_St * replicate(1., n_elements(AvSt))  ;* exp(-2.5d0*AvSt)
 fCO_St = fCO_St * replicate(1., n_elements(AvSt))  ;* exp(-1.7d0*AvSt)
 fH2_IS = fH2_IS * replicate(1., n_elements(AvIS))  ;* exp(-2.5d0*AvIS)
 fCO_IS = fCOss_IS * replicate(1., n_elements(AvIS))  ;* exp(-1.7d0*AvIS)

 OPENW, 01, envname

 PRINTF,01,'# ID x y z rho Tg  Td  G0  AvSt  AvIS  ZetaCR  ZetaX'$
            +' fH2_St  fCO_St fH2_IS  fCO_IS T00 T01 Hr  T0  TF'
 PRINTF,01,1
 PRINTF,01,n_elements(rho)

 for jj=0, n_elements(rho)-1 do begin

 PRINTF,01,Nt-1

 for i=1, Nt-1 do begin
   PRINTF,01,FORMAT = '(i8, TR2, 20(E11.4, TR2))',[ID[jj],x,y,z,rho[jj],Tg[jj],Td[jj],G0,AvSt,AvIS[jj],$
         ZetaCR,ZetaX,fH2_St,fCO_St,fH2_IS[jj],fCO_IS[jj],t[i-1,jj],t[i,jj],0d0,0d0,0d0]
 endfor

 endfor

 close,/all

end

;+
; PROGRAM Make_Environ_Semenov2010
;
; 07.2016
;
; This program sets up a grid of models to be compared to the chemical model
; benchmark of Semenov et al. 2010 for pre-stellar cores and disks
;
; Physical models:
;
; Model      T [K]     n(H) [cm-3]     Av [mag]         G0 [Draine]
; TMC1       10        2.00E+04           10               1
; Hot Core   100       2.00E+07           10               1
; Disk1      11.4      5.41E+08           37.1             428.3  # disk mid-plane
; Disk2      45.9      2.59E+07           1.94             393.2  # warm mol. layer
; Disk3      55.2      3.67E+06           0.22             353.5  # disk atmosphere
;
;+
@co-shield.pro
PRO Make_Environ_Semenov2010, envname
 if not keyword_set(envname) then envname = '1environ.inp'

 mp  = 1.6726d-24

 ZetaCR = 1.3d-17  ;Dima's version 1.3000e-17
 ZetaX  = 0.0d0
 G0     = 0.0d0
 Av_St  = 0.0d0
 fH2_St = 0.0d0
 fCO_St = 0.0d0

; test case B: constant physical parameters (from Tobias)
 t0 = 1.D-2          ; in years
 tf = [1E9,1E9,1E9,1E9,1E9]         ; in years
 Nt = 101
 dt1 = alog10(tf[0]-t0)/(Nt-1)
 t1 = 10.^(dindgen(Nt)*dt1) + t0
 dt2 = alog10(tf[1]-t0)/(Nt-1)
 t2 = 10.^(dindgen(Nt)*dt2) + t0
 dt3 = alog10(tf[2]-t0)/(Nt-1)
 t3 = 10.^(dindgen(Nt)*dt3) + t0

 t = [[t1],[t2],[t3],[t1],[t2]]

 ID = [1,2,3,4,5]
 x = 0d0
 y = 0d0
 z = 0d0

 G0 = [0d0,0d0,428.3,393.2,353.5]
 nH = [2.0E4,2.0E7,5.41E8,2.59E7,3.67E6]                   ; cm-3
 mu = 1.4
 rho= nH * mu * mp                ; in g cm-3
 Tg = [10.,100.,11.4,45.9,55.2]                     ; in K
 Td = Tg
; Shielding
 AvSt = [0.,0.,40.35,23.23,1.608]
 AvIS = [10.,10.,37.1,1.94,0.22]                   ; Av
 NtotIS = AvIS * 1.8699E21  ; cm^-2
 NcoIS = NtotIS * 1E-4
 Nh2IS = NtotIS * 0.5
 fH2_St = 1.
 fCO_St = 1.
 fH2_IS = 1. ;calculate_H2_self_shielding(Nh2IS)                  ; calculated for NH2 = 1E21
 fCOss_IS = 1. ;calculate_CO_self_shielding_factor(NcoIS) * calculate_CO_shielding_by_H2(Nh2IS)
                ; calculated for NH2 = 1E21, xCO = 1e-4

 fH2_St = fH2_St * replicate(1., n_elements(AvSt))  ;* exp(-2.5d0*AvSt)
 fCO_St = fCO_St * replicate(1., n_elements(AvSt))  ;* exp(-1.7d0*AvSt)
 fH2_IS = fH2_IS * replicate(1., n_elements(AvIS))  ;* exp(-2.5d0*AvIS)
 fCO_IS = fCOss_IS * replicate(1., n_elements(AvIS))  ;* exp(-1.7d0*AvIS)

 OPENW, 01, envname

 PRINTF,01,'# ID x y z rho Tg  Td  G0  AvSt  AvIS  ZetaCR  ZetaX'$
            +' fH2_St  fCO_St fH2_IS  fCO_IS T00 T01 Hr  T0  TF'
 PRINTF,01,1
 PRINTF,01,n_elements(rho)

 for jj=0, n_elements(rho)-1 do begin

 PRINTF,01,Nt-1

 for i=1, Nt-1 do begin
   PRINTF,01,FORMAT = '(i8, TR2, 20(E11.4, TR2))',[ID[jj],x,y,z,rho[jj],Tg[jj],Td[jj],G0[jj],AvSt[jj],AvIS[jj],$
         ZetaCR,ZetaX,fH2_St[jj],fCO_St[jj],fH2_IS[jj],fCO_IS[jj],t[i-1,jj],t[i,jj],0d0,0d0,0d0]
 endfor

 endfor

 close,/all

end

;+
; PROGRAM Make_Environ_Wakelam2015
;
; 07.2016
;
; This program sets up a grid of models to be compared to the chemical model
; benchmark of Wakelam et al. 2015 for the KIDA network.
;
; The models are set up similarly as in the Make_Environ_Semenov2010() function.
;
;+
@co-shield.pro
PRO Make_Environ_Wakelam2015, envname
 if not keyword_set(envname) then envname = '1environ.inp'

 mp  = 1.6726d-24

 ZetaCR = 1.3d-17  ;Dima's version 1.3000e-17
 ZetaX  = 0.0d0
 G0     = 0.0d0
 Av_St  = 0.0d0
 fH2_St = 0.0d0
 fCO_St = 0.0d0

; test case B: constant physical parameters (from Tobias)


t1 =   [0.0, 100.00000, 200.00000, 300.00000, 400.00000, 500.00000,  600.00000, $
        700.00000, 800.00000, 900.00000, 1000.0000, 1200.0000, 1300.0000, 1400.0000, $
        1600.0000, 1700.0000, 1900.0000, 2100.0000, 2400.0000, 2600.0000, 2900.0000, $
        3200.0000, 3500.0000, 3900.0000, 4300.0000, 4800.0000, 5300.0000, 5900.0000, $
        6500.0000, 7200.0000, 8000.0000, 8800.0000, 9800.0000, 10800.000, 12000.000, $
        13300.000, 14700.000, 16300.000, 18100.000, 20000.000, 22100.000, 24500.000, $
        27100.000, 30100.000, 33300.000, 36800.000, 40800.000, 45200.000, 50000.000, $
        55400.000, 61300.000, 67900.000, 75200.000, 83200.000, 92200.000, 102100.00, $
        113000.00, 125100.00, 138500.00, 153400.00, 169900.00, 188100.00, 208200.00, $
        230600.00, 255300.00, 282700.00, 313000.00, 346600.00, 383800.00, 424900.00, $
        470500.00, 521000.00, 576800.00, 638700.00, 707200.00, 783100.00, 867100.00, $
        960100.00, 1063000.0, 1177100.0, 1303300.0, 1443100.0, 1597900.0, 1769200.0, $
        1959000.0, 2169100.0, 2401800.0, 2659400.0, 2944600.0, 3260400.0, 3610100.0, $
        3997300.0, 4426100.0, 4900800.0, 5426400.0, 6008400.0, 6652900.0, 7366400.0, $
        8156500.0, 9031300.0, 10000000., 10630000.0, 11771000.0, 13033000.0, 14431000.0, $
       15979000.0, 17692000.0, 19590000.0, 21691000.0, 24018000.0, 26594000.0, 29446000.0, $
       32604000.0, 36101000.0, 39973000.0, 44261000.0, 49008000.0, 54264000.0, 60084000.0, $
       66529000.0, 73664000.0, 81565000.0, 90313000.0, 100000000.]

 Nt = n_elements(t1)

 t = [[t1],[t1],[t1],[t1],[t1],[t1],[t1],[t1],[t1]]

 ID = [20000,200000,20002,200002,20001,200001,200,2000,100000]
 x = 0d0
 y = 0d0
 z = 0d0

 G0 = [0d0,0d0,0d0,0d0,0d0,0d0,0d0,0d0,0d0]
 nH = [2.0E4,2.0E5,2.0E4,2.0E5,2.0E4,2.0E5,200.,2000.,1e5]                   ; cm-3
 mu = 1.4
 rho= nH * mu * mp                ; in g cm-3
 Tg = [10.,10.,10.,10.,20.,20.,20.,20.,10.]                     ; in K
 Td = Tg
; Shielding
 AvSt = [0.,0.,0.,0.,0.,0.,0.,0.,0.]
 AvIS = [30.,30.,2.,2.,1.,1.,1.,1.,10.]                   ; Av
 NtotIS = AvIS * 1.8699E21  ; cm^-2
 NcoIS = NtotIS * 1E-4
 Nh2IS = NtotIS * 0.5
 fH2_St = 1.
 fCO_St = 1.
 fH2_IS = calculate_H2_self_shielding(Nh2IS)                  ; calculated for NH2 = 1E21
 fCOss_IS = calculate_CO_self_shielding_factor(NcoIS) * calculate_CO_shielding_by_H2(Nh2IS)
                ; calculated for NH2 = 1E21, xCO = 1e-4

 fH2_St = fH2_St * [1.,1.,1.,1.,1.,1.,1.,1.,1.]
 fCO_St = fCO_St * [1.,1.,1.,1.,1.,1.,1.,1.,1.]
 fH2_IS = fH2_IS * [1.,1.,1.,1.,1.,1.,1.,1.,1.]
 fCO_IS = fCOss_IS * [1.,1.,1.,1.,1.,1.,1.,1.,1.]

 print, fH2_IS
 print, fCO_IS

 OPENW, 01, envname

 PRINTF,01,'# ID x y z rho Tg  Td  G0  AvSt  AvIS  ZetaCR  ZetaX'$
            +' fH2_St  fCO_St fH2_IS  fCO_IS T00 T01 Hr  T0  TF'
 PRINTF,01,1
 PRINTF,01,n_elements(rho)

 for jj=0, n_elements(rho)-1 do begin

 PRINTF,01,Nt-1

 for i=1, Nt-1 do begin
   PRINTF,01,FORMAT = '(i8, TR2, 20(E11.4, TR2))',[ID[jj],x,y,z,rho[jj],Tg[jj],Td[jj],G0[jj],AvSt[jj],AvIS[jj],$
         ZetaCR,ZetaX,fH2_St[jj],fCO_St[jj],fH2_IS[jj],fCO_IS[jj],t[i-1,jj],t[i,jj],0d0,0d0,0d0]
 endfor

 endfor

 close,/all

end

PRO Make_Environ_Bisbas2017, envname
 if not keyword_set(envname) then envname = '1environ.inp'

 mp  = 1.6726d-24

 ZetaCR = [1e-17,1e-16,1e-15,1e-14]
 ZetaX  = 0.0d0
 G0     = 0.0d0
 Av_St  = 0.0d0
 fH2_St = 0.0d0
 fCO_St = 0.0d0

; test case B: constant physical parameters (from Tobias)
 t0 = 1.D-2          ; in years
 tf = 1E9            ; in years
 Nt = 101
 dt = alog10(tf[0]-t0)/(Nt-1)
 t = 10.^(dindgen(Nt)*dt) + t0

 x = 0d0
 y = 0d0
 z = 0d0

 G0 = [0d0,0d0,0d0]
 nH = [1E2,1.0E3,1E4]
 mu = 1.4
 rho= nH * mu * mp                ; in g cm-3
 Tg = [[80.,21.,30.,35.],[12.,12.,23.,40.],[10.,10.,25.,45.]]                     ; in K
 Td = [[15.,21.,30.,35.],[12.,12.,23.,40.],[10.,10.,25.,45.]]                     ; in K
; Shielding
 AvSt = [0.,0.,0.,0.,0.,0.,0.,0.,0.]
 AvIS = [0.1,3.5,17.]                   ; Av
 NtotIS = AvIS * 1.8699E21  ; cm^-2
 NcoIS = NtotIS * 1E-4
 Nh2IS = NtotIS * 0.5
 fH2_St = 1.
 fCO_St = 1.
 fH2_IS = calculate_H2_self_shielding(Nh2IS)                  ; calculated for NH2 = 1E21
 fCOss_IS = calculate_CO_self_shielding_factor(NcoIS) * calculate_CO_shielding_by_H2(Nh2IS)
                ; calculated for NH2 = 1E21, xCO = 1e-4

 fH2_St = fH2_St * [1.,1.,1.,1.]
 fCO_St = fCO_St * [1.,1.,1.,1.]
 fH2_IS = fH2_IS * [1.,1.,1.,1.]
 fCO_IS = fCOss_IS * [1.,1.,1.,1.]

 print, fH2_IS
 print, fCO_IS

 OPENW, 01, envname

 PRINTF,01,'# ID x y z rho Tg  Td  G0  AvSt  AvIS  ZetaCR  ZetaX'$
            +' fH2_St  fCO_St fH2_IS  fCO_IS T00 T01 Hr  T0  TF'
 PRINTF,01,1
 PRINTF,01,n_elements(rho)*n_elements(ZetaCR)

 for jj=0, n_elements(rho)-1 do begin
     for kk=0, n_elements(ZetaCR)-1 do begin
        ID = jj+kk
        PRINTF,01,Nt-1
        for i=1, Nt-1 do begin
            PRINTF,01,FORMAT = '(i8, TR2, 20(E11.4, TR2))',  $
                 [ID,x,y,z,rho[jj],Tg[kk,jj],Td[kk,jj],G0[jj],AvSt[jj],AvIS[jj],$
                 ZetaCR[kk],ZetaX,fH2_St[jj],fCO_St[jj],fH2_IS[jj],fCO_IS[jj], $
                 t[i-1],t[i],0d0,0d0,0d0]
        endfor
     endfor
 endfor

 close,/all

end


;+
; PROGRAM Make_Environ_GMC
;
; 05.2016
;
; This program sets up a grid of models to be compared to the chemical models
; of Sipila et al. 2015 for protostellar cores
;
; Parameters:
;    density: 300, 3 000 and 30 000 particle per cm^-3
;    Av: 10 mag
;    Tg = Td: 10 K
;    ZetaCR: 1.3D-17 s^-1
;    rho: 1E3, 1E4, 1E5, 1E6
; The (self-) shielding factors are calculated asuming a fixed Av to N(H)
; conversion factor and fixed abundances.
;+
@co-shield.pro
PRO Make_Environ_GMC, envname
 if not keyword_set(envname) then envname = '1environ_GMC.inp'

 mp  = 1.6726d-24

 ZetaCR = 1.3d-17  ;Dima's version 1.3000e-17
 ZetaX  = 0.0d0
 G0     = 0.0d0
 Av_St  = 0.0d0
 fH2_St = 0.0d0
 fCO_St = 0.0d0

; test case B: constant physical parameters (from Tobias)
 t0 = 1.D-2          ; in years
 tf = 1E7         ; in years
 Nt = 101
 dt = alog10(tf[0]-t0)/(Nt-1)
 t = 10.^(dindgen(Nt)*dt) + t0

 ID = [1,2,3,4,41]
 x = 0d0
 y = 0d0
 z = 0d0

 G0 = 0d0
 nH = [1e2,1e3,1e4,1e5,1e5]                   ; cm-3
 mu = 1.4  ;2.3197418
 rho= nH * mu * mp                ; in g cm-3
 Tg = [34.,12.,10.,8.5,10.]                     ; in K
 Td = [12.,10.,10.,7.7,10.]                     ; in K
; Shielding
 AvSt = 0.
 AvIS = [0.8,2.,3.2,7.,10.]                   ; Av
 NtotIS = AvIS * 1.8699E21  ; cm^-2
 NcoIS = NtotIS * 1E-4
 Nh2IS = NtotIS * 0.5
 fH2_St = 1.
 fCO_St = 1.
 fH2_IS = calculate_H2_self_shielding(Nh2IS)                  ; calculated for NH2 = 1E21
 fCOss_IS = calculate_CO_self_shielding_factor(NcoIS) * calculate_CO_shielding_by_H2(Nh2IS)
                ; calculated for NH2 = 1E21, xCO = 1e-4

 fH2_St = fH2_St * replicate(1., n_elements(AvSt))  ;* exp(-2.5d0*AvSt)
 fCO_St = fCO_St * replicate(1., n_elements(AvSt))  ;* exp(-1.7d0*AvSt)
 fH2_IS = fH2_IS * replicate(1., n_elements(AvIS))  ;* exp(-2.5d0*AvIS)
 fCO_IS = fCOss_IS * replicate(1., n_elements(AvIS))  ;* exp(-1.7d0*AvIS)

 print, fCOss_IS

 OPENW, 01, envname

 PRINTF,01,'# ID x y z rho Tg  Td  G0  AvSt  AvIS  ZetaCR  ZetaX'$
            +' fH2_St  fCO_St fH2_IS  fCO_IS T00 T01 Hr  T0  TF'
 PRINTF,01,1
 PRINTF,01,n_elements(rho)

 for jj=0, n_elements(rho)-1 do begin

 PRINTF,01,Nt-1

 for i=1, Nt-1 do begin
   PRINTF,01,FORMAT = '(i8, TR2, 20(E11.4, TR2))',[ID[jj],x,y,z,rho[jj],Tg[jj],Td[jj],G0,AvSt,AvIS[jj],$
         ZetaCR,ZetaX,fH2_St,fCO_St,fH2_IS[jj],fCO_IS[jj],t[i-1],t[i],0d0,0d0,0d0]
 endfor

 endfor

 close,/all

end
